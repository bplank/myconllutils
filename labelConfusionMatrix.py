import argparse
import sys
from collections import Counter


parser = argparse.ArgumentParser(description="Extracts relations from syntactic- or dependency-parsed input")
parser.add_argument("file1",   metavar="FILE", help="name of the parsedfile file")
parser.add_argument("file2", metavar="FILE", help="name of the parsedfile file")
parser.add_argument("-lc","--labelcolumnindex", help="column index of the label (dependency or POS), used with -t regular and -t headposm, default -1", required=False, type=int, default=-1) #column index as array indices, starting from 0
parser.add_argument("-hc","--headcolumindex", required=False, help="column index of the head, used with -t headpos and -t headdistance", type=int, default=8) #column index as array indices, starting from 0
parser.add_argument("-t","--typeofmatrix", help="""headdistance (distance between dependent and head in tokens)
                                                 regular (confusion matrix between any labels)
                                                 regularD (confusion matrix between any labels plus directionality
                                                 headposD (headpos+directionality)
                                                 headpos (the POS tag of the need, specify POS column with -c and head column with -h )""", required=False, default="regular")  # headdistance, regular, headpos
parser.add_argument("-m","--maxdist", required=False, type=int) #only valid for headdistance, keeps the size of the matrix between [-maxdist, maxdist]



args = parser.parse_args()


def getHeadPOSarray(file,headcolumn,poscolumn): #This function is REALLY PATCHY!!
    acc = []
    H = {}
    H["0"] = "ROOT"
    current_heads = []
    for line in open(file).readlines():
        if len(line) < 2:
            for head_i in current_heads:
                #if int(head_i) > len(current_heads) or int(head_i) < 0: #This is a patch for DDT
                #    head_i = "0"
                acc.append(H[head_i])
            H = {}
            H["0"] = "ROOT"
            current_heads = []
        else:
            line = line.strip().split("\t")
            current_heads.append(line[headcolumn])
            H[line[0]] = line[poscolumn]

    for head_i in current_heads:
                acc.append(H[head_i])
    return acc

def getHeadIndexarray(file,headcolumn): #This function is REALLY PATCHY!!
    acc = []

    current_heads = []
    for line in open(file).readlines():
        if len(line) < 2:
            for head_i in current_heads:
                acc.append(head_i)
            current_heads = []
        else:
            line = line.strip().split("\t")
            current_heads.append(line[headcolumn])
    for head_i in current_heads:
                acc.append(head_i)
    return acc

def getColumn(file, column):
    acc = []
    for line in open(file):
        if len(line) < 2:
            pass
        else:
            acc.append(line.strip().split("\t")[column])
    return acc

def applymaxdist(array,maxdist):
    acc = []
    for v in array:
        if v > -1*maxdist and v < maxdist:
            acc.append(v)
        elif v <= -1*maxdist:
            acc.append(-1*maxdist)
        #elif v >= args.maxdist:
        else:
            acc.append(maxdist)
    return acc


def rowtotals(M,L):
    R = Counter()
    for label in L:
        for (row, col) in M.iterkeys():
            if row == label:
                R[label]+=1.0*M[(row,col)]
    return R

def coltotals(M,L):
    C = Counter()
    for label in L:
        for row, col in M.iterkeys():
            if col == label:
                C[label]+=1.0*M[row,col]
    return C


def main():

    M = Counter()
    L = set()
    T = 0

    if args.typeofmatrix == "regular":

        labels1 = getColumn(args.file1, args.labelcolumnindex)
        labels2 = getColumn(args.file2, args.labelcolumnindex)

        for l1, l2 in zip(labels1,labels2):
            L.add(l1)
            L.add(l2)
            M[(l1,l2)]+=0.5
            M[(l2,l1)]+=0.5


    elif args.typeofmatrix == "headdistance": #the values for the confusion matrix are the distances to the head. It could be used in other terms like f.i. standard deviation or building a covariance matrix
        #this confusion matrices get too large, and more importantly, differently-sized. Consider

        heads1 = [int(x) - int(y) for x,y in zip(getColumn(args.file1, args.headcolumindex), getColumn(args.file1, 0))]
        heads2 = [int(x) - int(y) for x,y in zip(getColumn(args.file2, args.headcolumindex), getColumn(args.file2, 0))]

        if args.maxdist: #keeps the size of the matrix between [-maxdist, maxdist]
            heads1 = applymaxdist(heads1,args.maxdist)
            heads2 = applymaxdist(heads2,args.maxdist)

        for h1, h2 in zip(heads1,heads2):
            L.add(h1)
            L.add(h2)
            M[(h1,h2)]+=0.5
            M[(h2,h1)]+=0.5

    elif args.typeofmatrix == "headpos":
        poscol = args.labelcolumnindex
        headcol = args.headcolumindex
        heads1 = getHeadPOSarray(args.file1, headcol, poscol)
        heads2 = getHeadPOSarray(args.file2, headcol, poscol)
        for h1, h2 in zip(heads1,heads2):
            L.add(h1)
            L.add(h2)
            M[(h1,h2)]+=0.5
            M[(h2,h1)]+=0.5
    elif args.typeofmatrix == "headposD":
        poscol = args.labelcolumnindex
        headcol = args.headcolumindex
        heads1 = getHeadPOSarray(args.file1, headcol, poscol)
        heads2 = getHeadPOSarray(args.file2, headcol, poscol)
        hindex1 = getHeadIndexarray(args.file1, headcol)
        hindex2 = getHeadIndexarray(args.file2, headcol)
        windex1 = getColumn(args.file1, 0)
        windex2 = getColumn(args.file2, 0)
        for h1, h2,hi1,hi2,wi1,wi2 in zip(heads1,heads2,hindex1,hindex2,windex1,windex2):
            suf1="@R"
            suf2="@R"
            if wi1 > hi1: #left attachment
                suf1="@L"
            if wi2 > hi2: #left attachment
                suf2="@L"

            h1=h1+suf1
            h2=h2+suf2
            L.add(h1)
            L.add(h2)
            M[(h1,h2)]+=0.5
            M[(h2,h1)]+=0.5
        #processing for ROOTS to the right and OOBS
        for h1, h2,hi1,hi2,wi1,wi2 in zip(heads1,heads2,hindex1,hindex2,windex1,windex2):
            suf1="@R"
            suf2="@R"
            if wi1 > hi1: #left attachment
                suf1="@L"
            if wi2 > hi2: #left attachment
                suf2="@L"

            h1=h1+suf1
            h2=h2+suf2


            if h1 == "ROOT@L" and h2=="ROOT@L":
                h1= "ROOT@R"
                h2= "ROOT@R"
                L.add(h1)
                L.add(h2)
                M[(h1,h2)]+=0.5
                M[(h2,h1)]+=0.5
                h1= "OOB@L"
                h2= "OOB@L"
                L.add(h1)
                L.add(h2)
                M[(h1,h2)]+=0.5
                M[(h2,h1)]+=0.5

            elif h1 == "ROOT@L":

                h1= "ROOT@R"
                L.add(h1)
                L.add(h2)
                M[(h1,h2)]+=0.5
                M[(h2,h1)]+=0.5
                h1= "OOB@L"
                L.add(h1)
                L.add(h2)
                M[(h1,h2)]+=0.5
                M[(h2,h1)]+=0.5
            elif h2 == "ROOT@L":
                h2= "ROOT@R"
                L.add(h1)
                L.add(h2)
                M[(h1,h2)]+=0.5
                M[(h2,h1)]+=0.5
                h2= "OOB@R"
                L.add(h1)
                L.add(h2)
                M[(h1,h2)]+=0.5
                M[(h2,h1)]+=0.5
    elif args.typeofmatrix == "regularD":
        headcol = args.headcolumindex
        labels1 = getColumn(args.file1, args.labelcolumnindex)
        labels2 = getColumn(args.file2, args.labelcolumnindex)
        hindex1 = getHeadIndexarray(args.file1, headcol)
        hindex2 = getHeadIndexarray(args.file2, headcol)
        windex1 = getColumn(args.file1, 0)
        windex2 = getColumn(args.file2, 0)
        for l1, l2,hi1,hi2,wi1,wi2 in zip(labels1,labels2,hindex1,hindex2,windex1,windex2):
            suf1="@R"
            suf2="@R"
            if wi1 > hi1: #left attachment
                suf1="@L"
            if wi2 > hi2: #left attachment
                suf2="@L"
            l1=l1+suf1
            l2=l2+suf2
            L.add(l1)
            L.add(l2)
            M[(l1,l2)]+=0.5
            M[(l2,l1)]+=0.5


    #even the diagonal to 0s
    #for l in L:
    #    M[(l,l)]=0


    print "#"+" ".join(sys.argv)
    print "#row, col, support, norm_over_total, norm_over_row_i, norm_over_row_i_and_col_j"
    T = sum(M.values())
    rowtots = rowtotals(M,L)
    coltots= coltotals(M,L)
    #print rowtots
    #print coltots
    for l1 in sorted(L):
        for l2 in sorted(L):
            if M[l1,l2] == 0.0:
                outline = [l1, l2, 0.0, 0.0, 0.0, 0.0 ]
            else:
                outline = [l1, l2, M[(l1,l2)],M[(l1,l2)]*1.0/T, M[(l1,l2)]/rowtots[l1], M[(l1,l2)]/(rowtots[l1] + coltots[l2])]
            print "\t".join([str(x) for x in outline])


main()