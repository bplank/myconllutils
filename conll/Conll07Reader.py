import sys
import re
import math
class Conll07Reader:
    ### read Conll 2007 data
    ### http://nextens.uvt.nl/depparse-wiki/DataFormat

    def __init__(self,filename,stdin=False):
        self.filename = filename
        if stdin:
            self.FILE=sys.stdin
        else:
            self.FILE = open(self.filename,"r")

    def getNext(self):
        # return next instance or None

        line = self.FILE.readline()

        line = line.strip()
        lineList = line.split("\t")

        ids = []
        form = []
        lemma = []
        cpos = []
        pos = []
        feats = []
        head = []
        deprel = []
        phead = []
        pdeprel = []

        if len(lineList) == 10:
            # contains all cols, also phead/pdeprel
            while len(lineList) == 10:
                ids.append(int(lineList[0]))
                form.append(lineList[1])
                lemma.append(lineList[2])
                cpos.append(lineList[3])
                pos.append(lineList[4])
                feats.append(lineList[5])
                head.append(int(lineList[6]))
                deprel.append(lineList[7])
                # use _ if last two columns contain a -
                phead.append("_" if lineList[8] == "-" else lineList[8])
                pdeprel.append("_" if lineList[9] == "-" else lineList[9])

                line = self.FILE.readline()
                line = line.strip()
                lineList = line.split("\t")
        elif len(lineList) == 8:
            while len(lineList) == 8:
                ids.append(int(lineList[0]))
                form.append(lineList[1])
                lemma.append(lineList[2])
                cpos.append(lineList[3])
                pos.append(lineList[4])
                feats.append(lineList[5])
                head.append(int(lineList[6]))
                deprel.append(lineList[7])
                phead.append("_")
                pdeprel.append("_")

                line = self.FILE.readline()
                line = line.strip()
                lineList = line.split("\t")
        elif len(lineList) == 9:
            while len(lineList) == 9:
                ### line after deprel contains possible Wiktionary tags separated by comma, conflate them and use as cpos 
                ids.append(int(lineList[0]))
                #form.append(lineList[1])
                form.append("_") #remove token
                lemma.append(lineList[2])
                cpos.append("|".join(set(lineList[8].split(","))))
                pos.append("_") #remove fpos
                feats.append(lineList[5])
                head.append(int(lineList[6]))
                deprel.append(lineList[7])
                phead.append("_")
                pdeprel.append("_")

                line = self.FILE.readline()
                line = line.strip()
                lineList = line.split("\t")

        elif len(lineList) > 1:
            raise Exception("not in right format!")


        if len(form) > 0: 
            return DependencyInstance(ids,form,lemma,cpos,pos,feats,head,deprel,phead,pdeprel)
        else: 
            return None


    def getInstances(self):
        instance = self.getNext()
        instances = []
        while instance:
            instance.setIdx(len(instances)-1)
            instances.append(instance)

            instance = self.getNext()
        return instances 

    def getSentences(self):
        """ return sentences as list of lists """
        instances = self.getInstances()
        sents = []
        for i in instances:
            sents.append(i.form)
        return sents

   


class DependencyInstance:
    
    def __init__(self, ids, form, lemma, cpos, pos, feats, headid, deprel, phead, pdeprel):
        #self.idx is set during reading
        self.ids = ids
        self.form = form
        self.lemma = lemma
        self.cpos = cpos
        self.pos = pos
        self.feats = feats
        self.headid = headid
        self.deprel = deprel
        self.phead = phead
        self.pdeprel = pdeprel       
        self.FUNC = ["aux","auxpass","case","cc","cop","det","mark","neg"]
        self.CORE = ["ccomp", "csubj","csubjpass","dobj","iobj","nsubj","nsubjpass","xcomp"]
        self.NON_CORE = ["acl","advcl","advmod","amod","appos","compound","conj","dep","discourse","dislocated","expl","foreign","goeswith","list","mwe","name","nmod","nummod","parataxis","remnant","reparandum","root","vocative"]
        self.PUNCT = ["punct"]
        
    def __str__(self):
        s = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\n"
        sout = ""
        for i in range(len(self.form)):
            sout += s.format(self.ids[i],self.form[i],self.lemma[i],self.cpos[i],self.pos[i],self.feats[i],self.headid[i],self.deprel[i],self.phead[i],self.pdeprel[i])
        return sout

    def setIdx(self,idx):
        self.idx=idx

    def getIdx(self):
        return self.idx

    def removeIdFeat(self,all=False):
        if all:
            self.feats=["_" for x in self.feats]
            return 
        # self.feat contains list of feats for entiry instance, need to split by "|" first
        ffeats=[fidx.split("|") for fidx in self.feats]
        newfeats=[]
        for featlist in ffeats:
            newfeat=[]
            for val in featlist:
                if not val.startswith("id="):
                    newfeat.append(val)
            if not newfeat: #if no feat remains
                newfeats.append("_")
            else:
                newfeats.append("|".join(newfeat))
        self.feats = newfeats

    def lowercaseRoot(self):
        for i,deprel in enumerate(self.deprel):
            if deprel=="ROOT":
                self.deprel[i]=self.deprel[i].lower()

    def getCoreDeprelCount(self,what="core"): #,"func","non_core"):
        """ return core relations in instance and total relations (length) """
        if what == "core":
            rels = [deprel for deprel in self.deprel if deprel in self.CORE] 
        elif what == "func":
            rels = [deprel for deprel in self.deprel if deprel in self.FUNC]
        elif what =="non_core":
            rels = [deprel for deprel in self.deprel if deprel in self.NON_CORE]
        elif what == "punct":
            rels = [deprel for deprel in self.deprel if deprel in self.PUNCT]
        else:
            print("unknown option")
            exit()
        return len(rels), len(self.deprel), rels

    def reattachRoot(self):
        """
        if tree has multiple roots, reattach to leftmost dep
        """
        containsMultipleRoots=False
        numRoots=0
        for i,deprel in enumerate(self.deprel):
            if deprel=="ROOT":
                numRoots+=1
                if numRoots == 1:
                    maindep=self.ids[i]
                else:
                    containsMultipleRoots=True
                    # reattach to dep of left-most root (keep root label)
                    self.headid[i]=maindep
        return containsMultipleRoots
            


    #### COLUMNS:
    # ID FORM LEMMA PLEMMA POS PPOS FEAT PFEAT HEAD PHEAD DEPREL PDEPREL FILLPRED PRED APREDs
    # ID FORM LEMMA        POS      FEAT       HEAD       DEPREL are the same as in the CoNLL-2006 and CoNLL-2007
    
    def printConll09(self,poscolidx=3,removeFeats=False,removeWord=False,removeLemma=False):
        s = "{0}\t{1}\t{2}\t_\t{3}\t_\t{4}\t_\t{5}\t_\t{6}\t{7}\t{8}\t_\n"
        sout = ""
        ### decide which POS column to take
        if poscolidx==3:
            thepos=self.cpos
        elif poscolidx==4:
            thepos=self.pos
        else:
            print("poscolidx not supported")
            exit()
        for i in range(len(self.form)):
            word=self.form[i]
            lemma=self.lemma[i]
            feats=self.feats[i]
            if removeWord:
                word="_"
            if removeFeats:
                feats="_"
            if removeLemma:
                lemma="_"
            sout += s.format(self.ids[i],word,lemma,thepos[i],feats,self.headid[i],self.deprel[i],self.phead[i],self.pdeprel[i])
        return sout

    def __repr__(self):
        return self.__str__()

    def equalForm(self,instance):
        for f1,f2 in zip(self.form,instance.form):
            if f1 != f2:
#            if f1 != "<num>" and f2 != "<num>" and f1 != f2:
                return False
        return True

    def sameParse(self,instance,ignorePunct=False):
        for h1,h2,l1,l2 in zip(self.headid,instance.headid,self.deprel,instance.deprel):
            if h1 != h2:
                if ignorePunct and (l1 in ["punct","pnct"] or l2 in ["punct","pnct"]):
                    continue
                return False
        return True
        #return self.equalHeads(instance) and self.equalLabels(instance,ignorePunct)
            

    def maxTreeDepth(self):
        """
        find max depth of tree (go over all tokens and find longest path to root)
        """
        maxDistance=0
        for wid in self.ids:
            dist=self.distanceToRoot(wid)
            if dist>maxDistance:
                maxDistance=dist
        return maxDistance

    def LAS(self,instance,ignorePunct=False,b=False):
        alltokens,uas,las,correctHead,correctHeadDep,scoringtokens = self.evaluate(instance,ignorePunct=ignorePunct,b=b)
        return las

    def LASbyDepth(self,instance,ignorePunct=False,b=False):
        correctHead=0.0
        correctHeadDep=0.0
        scoringtokens=0.0
        alltokens=0.0
        
        
        for word,wid,h1,h2,l1,l2 in zip(self.form,self.ids,self.headid,instance.headid,self.deprel,instance.deprel):
            #weight=1/float(self.distanceToRoot(wid))
            weight=1/float(1.0)+math.log(self.distanceToRoot(wid))
            alltokens+=weight
            # LAS ignores punctuation on the token-level (not pos)
            if ignorePunct and self.isPunctuationToken(word):
            #    print "ignore ", word
                continue #continue if token matches a punctuation symbol (to approx perl script eval07)
            if h1==h2 and l1==l2:
                correctHeadDep+=weight
            if h1==h2:
                correctHead+=weight
            scoringtokens+=weight
        if ignorePunct:
            if scoringtokens == 0:
                las=0.0
                uas=0.0
            else:
                las = correctHeadDep/scoringtokens *100
                uas = correctHead/scoringtokens*100
        else:
            las = correctHeadDep/alltokens *100
            uas = correctHead/alltokens* 100
        if b:
            print(("\t{6}\t{0:.0f}\t0\t{1:.2f}\t{2:.2f}\t{3:.0f}\t{4:.0f}\t{5:.0f} 0 0 0 0".format(alltokens,uas,las,correctHead,correctHeadDep,scoringtokens,self.idx+1)))

        return alltokens,uas,las,correctHead,correctHeadDep,scoringtokens


    def LASwPOS(self,instance,ignorePunct=False,b=False):
        """
        LAS in which certain POS weigh more
        """
        correctHead=0.0
        correctHeadDep=0.0
        scoringtokens=0.0
        alltokens=0.0
        
        
        for word,wid,h1,h2,l1,l2,gcpos in zip(self.form,self.ids,self.headid,instance.headid,self.deprel,instance.deprel,instance.cpos):
            #weight=1.0
            weight=1/float(1.0)+math.log(self.distanceToRoot(wid))
            if gcpos in ["NOUN","VERB","ADJ", "PROPN"]:
                weight=1.0+weight
            
            alltokens+=weight
            # LAS ignores punctuation on the token-level (not pos)
            if ignorePunct and self.isPunctuationToken(word):
            #    print "ignore ", word
                continue #continue if token matches a punctuation symbol (to approx perl script eval07)
            if h1==h2 and l1==l2:
                correctHeadDep+=weight
            if h1==h2:
                correctHead+=weight
            scoringtokens+=weight
        if ignorePunct:
            if scoringtokens == 0:
                las=0.0
                uas=0.0
            else:
                las = correctHeadDep/scoringtokens *100
                uas = correctHead/scoringtokens*100
        else:
            las = correctHeadDep/alltokens *100
            uas = correctHead/alltokens* 100
        if b:
            print(("\t{6}\t{0:.0f}\t0\t{1:.2f}\t{2:.2f}\t{3:.0f}\t{4:.0f}\t{5:.0f} 0 0 0 0".format(alltokens,uas,las,correctHead,correctHeadDep,scoringtokens,self.idx+1)))

        return alltokens,uas,las,correctHead,correctHeadDep,scoringtokens


    def complete_predicates(self,instance,labeled=True,ignorePunct=False,b=False,soft=False):
        """
        inspired by semantic dependency parsing SemEval 15 metric; 
        how many of the core predicates (have V as head) are completely right (labeled and complete)
        also outputs unlabeled complete predicates (ignores label on edge)
        soft: allow soft matches (macro-average over per-frame valency accuracy)
        """
        corepredicates=0.0  #scoring and total are equal here
        complete_predicates=0.0
        complete_predicates_unlabeled=0.0

        debug=0
        for word,h1,h2,l1,l2,cpos,wid in zip(self.form,self.headid,instance.headid,self.deprel,instance.deprel,instance.cpos,instance.ids):
            # if node represents a scorable predicate (we here only know about VERB no explicit pred marking), take from gold!
            if cpos.startswith("V"): #goldpos is verb
                # get children of current node
                predChildsLab=self.getChildIds(wid,ignorePunct=ignorePunct,labeled=True)
                goldChildsLab=instance.getChildIds(wid,ignorePunct=ignorePunct,labeled=True)

                predChildsULab=self.getChildIds(wid,ignorePunct=ignorePunct,labeled=False)
                goldChildsULab=instance.getChildIds(wid,ignorePunct=ignorePunct,labeled=False)

                if not goldChildsLab:
                    continue #ignore predicates with no arguments

                if debug:
                    print(instance.getIdx())
                    print(wid,word,cpos, "predChildsLab:", predChildsLab, "goldLab:", goldChildsLab)
                    if set(predChildsLab)==set(goldChildsLab):
                        print("CoRRECT")
                    else:
                        print("WRoNG")
                        print()
                if not soft: #complete predicate must match
                    if set(predChildsLab)==set(goldChildsLab):
                        complete_predicates+=1
                    if set(predChildsULab)==set(goldChildsULab):
                        complete_predicates_unlabeled+=1
                else: 
                    # allow soft matches
                    intersect=len(set(predChildsLab).intersection(set(goldChildsLab))) 
                    complete_predicates += intersect/float(len(set(goldChildsLab)))

                    intersectU=len(set(predChildsULab).intersection(set(goldChildsULab))) 
                    complete_predicates_unlabeled+= intersectU/float(len(set(goldChildsULab)))

                corepredicates+=1.0
                    
       
        lcp=complete_predicates/corepredicates if corepredicates>0 else 0.0
        ucp=complete_predicates_unlabeled/corepredicates if corepredicates>0 else 0.0

        if b:
            print(("\t{6}\t{0:.0f}\t0\t{1:.2f}\t{2:.2f}\t{3:.0f}\t{4:.0f}\t{5:.0f} 0 0 0 0".format(corepredicates,ucp,lcp,complete_predicates_unlabeled,complete_predicates,corepredicates,self.idx+1)))

        #return alltokens,uas,las,correctHead,correctHeadDep,scoringtokens
        return corepredicates,ucp,lcp,complete_predicates_unlabeled,complete_predicates,corepredicates


    def isPunctuationToken(self,word):
        """
        whether token is punctuation (matches eval07 perl punct class)
        """
        return re.match(r"^[.,;:/?!\_\-\"'\[\]@\*&#%]+$",word)

    def evaluate(self,instance,ignorePunct=False,b=False): # eval-b option to print
        correctHead=0.0
        correctHeadDep=0.0
        scoringtokens=0.0
        alltokens=0.0
        
        for word,h1,h2,l1,l2 in zip(self.form,self.headid,instance.headid,self.deprel,instance.deprel):
            alltokens+=1
            # LAS ignores punctuation on the token-level (not pos)
            if ignorePunct and self.isPunctuationToken(word):
            #    print "ignore ", word
                continue #continue if token matches a punctuation symbol (to approx perl script eval07)
            if h1==h2 and l1==l2:
                correctHeadDep+=1
            if h1==h2:
                correctHead+=1
            scoringtokens+=1
        if ignorePunct:
            if scoringtokens == 0:
                las=0.0
                uas=0.0
            else:
                las = correctHeadDep/scoringtokens *100
                uas = correctHead/scoringtokens*100
        else:
            las = correctHeadDep/alltokens *100
            uas = correctHead/alltokens* 100
        if b:
            print(("\t{6}\t{0:.0f}\t0\t{1:.2f}\t{2:.2f}\t{3:.0f}\t{4:.0f}\t{5:.0f} 0 0 0 0".format(alltokens,uas,las,correctHead,correctHeadDep,scoringtokens,self.idx+1)))

        return alltokens,uas,las,correctHead,correctHeadDep,scoringtokens


    def evaluateCNC(self,instance,b=False): # eval-b option to print
        #instance is assumed to be gold!
        correctHead=0.0
        correctHeadDep=0.0
        scoringtokens=0.0
        alltokens=0.0
        
        for word,h1,h2,l1,l2 in zip(self.form,self.headid,instance.headid,self.deprel,instance.deprel):
            alltokens+=1
            # LAS ignores punctuation on the token-level (not pos)
            if l2 in ["punct", "aux", "auxpass", "case", "cc", "cop", "det", "mark", "neg"]:
                continue #continue on punct and funct deprels!
            if h1==h2 and l1==l2:
                correctHeadDep+=1
            if h1==h2:
                correctHead+=1
            scoringtokens+=1
        
        if scoringtokens == 0:
            las = 0.0 # for instance in german " in dem hintergrund " only root attachment thus 0
            uas = 0.0
        else:
            las = correctHeadDep/scoringtokens *100
            uas = correctHead/scoringtokens* 100
        if b:
            print(("\t{6}\t{0:.0f}\t0\t{1:.2f}\t{2:.2f}\t{3:.0f}\t{4:.0f}\t{5:.0f} 0 0 0 0".format(alltokens,uas,las,correctHead,correctHeadDep,scoringtokens,self.idx+1)))

        return alltokens,uas,las,correctHead,correctHeadDep,scoringtokens

    def attachesToMainVerb(self, tokenid):
        """
        return true if the current token is a child of the main verb
        """
        if tokenid==0:
            return False #since head
        head=self.headid[tokenid-1]
        mainChilds=self.getChildIds(0)
        return head in mainChilds


    def distanceToRoot(self, tokenid):
        """
        return how many edges away we are from root, if it attaches to root distance=1
        """
        distance=0
        if tokenid==0:
            return distance #is root
        head=self.headid[tokenid-1]
        if head==0:
            return distance+1
        while head != 0:
            head = self.headid[tokenid-1]
            distance+=1
            tokenid =head  
        return distance

    def getChildIds(self, tokenid,ignorePunct=False,labeled=True):
        """
        return ids of children of given token (find word ids who have tokenid as head)
        if labeled: return tuples of (childId,label) else only childid
        """
        childIds=[]
        for wid,word,headid in zip(self.ids,self.form,self.headid):
            if headid==tokenid:
                if ignorePunct and self.isPunctuationToken(word): 
                    continue
                childIds.append(wid) if not labeled else childIds.append((wid,self.deprel[wid-1]))
                
        return childIds

    def getLeftChildIds(self, tokenid,ignorePunct=False):
        """
        return ids of left children of given token (find word ids who have tokenid as head)
        """
        childIds=[]
        for wid,headid in zip(self.ids,self.headid):
            if headid==tokenid and wid < tokenid:
                if ignorePunct and self.isPunctuationToken(word): 
                    continue
                childIds.append(wid)
        return childIds

    def getRightChildIds(self, tokenid,ignorePunct=False):
        """
        return ids of right children of given token (find word ids who have tokenid as head)
        """
        childIds=[]
        for wid,headid in zip(self.ids,self.headid):
            if headid==tokenid and wid > tokenid:
                if ignorePunct and self.isPunctuationToken(word): 
                    continue
                childIds.append(wid)
        return childIds


    def evaluateNED(self,gold,ignorePunct=False,b=False): # eval-b option to print
        """
        NED (neutralizing edge direction) Schwartz et al., 2011
        considers unlabeled attachment
        attachment is correct if the token's parent is (1) its gold parent or (2) its gold child
        """
        correctHead=0.0
        correctHeadDep=0.0
        scoringtokens=0.0
        alltokens=0.0

        gc=False

        for word,wid,h1,h2,l1,l2 in zip(self.form,self.ids,self.headid,gold.headid,self.deprel,gold.deprel):
            alltokens+=1
            if ignorePunct and self.isPunctuationToken(word):
                continue #continue if token matches a punctuation symbol (should be identical to regex in perl eval script)

            #h2 = gold head
            goldChilds=gold.getChildIds(wid)
            #print goldChilds

            if h1 in goldChilds:
                gc=True
            #    print "yes", h1, goldChilds, self.form
            if h1==h2 or h1 in goldChilds:
                correctHead+=1
            scoringtokens+=1
        if ignorePunct:
            if scoringtokens == 0:
                las=0.0
                uas=0.0
            else:
                las = correctHeadDep/scoringtokens *100
                uas = correctHead/scoringtokens*100
        else:
            las = correctHeadDep/alltokens *100
            uas = correctHead/alltokens* 100
        if b:
            # gc = whether back-off to goldchild
            print(("\t{7}\t{0:.0f}\t0\t{1:.2f}\t{2:.2f}\t{3:.0f}\t{4:.0f}\t{5:.0f} 0 0 0 {6}".format(alltokens,uas,las,correctHead,correctHeadDep,scoringtokens,gc,self.idx+1)))

        return alltokens,uas,las,correctHead,correctHeadDep,scoringtokens


    def evaluateLA(self,instance,ignorePunct=False,b=False): # eval-b option to print
        """
        label attachment
        """
        correctDep=0.0
        scoringtokens=0.0
        alltokens=0.0
        
        for word,wid,h1,h2,l1,l2 in zip(self.form,self.ids,self.headid,instance.headid,self.deprel,instance.deprel):
            alltokens+=1
            if ignorePunct and self.isPunctuationToken(word):
                continue #continue if token matches a punctuation symbol (to approx perl script eval07)
            if l1==l2:
                correctDep+=1.0
            scoringtokens+=1.0
        if ignorePunct:
            if scoringtokens == 0:
                la=0.0
            else:
                la = correctDep/scoringtokens *100
        else:
            la = correctDep/alltokens *100
        if b:
            print(("\t{6}\t{0:.0f}\t0\t{1:.2f}\t{2:.2f}\t{3:.0f}\t{4:.0f}\t{5:.0f} 0 0 0 0".format(alltokens,la,la,correctDep,correctDep,scoringtokens,self.idx+1)))

        return alltokens,la,la,correctDep,correctDep,scoringtokens

    def evaluateLAforPOS(self,instance,poslist=["NOUN"],ignorePunct=False,b=False): # eval-b option to print
        """
        label attachment score counting only the tokens that have a certain pos value --- taken from instance.cpos, this might cause disturbance if there is pos mismatch between gold and self
        """
        correctDep=0.0
        scoringtokens=0.0
        alltokens=0.0

        for word,wid,h1,h2,l1,l2,goldpos in zip(self.form,self.ids,self.headid,instance.headid,self.deprel,instance.deprel,instance.cpos):
            alltokens+=1
            if ignorePunct and self.isPunctuationToken(word):
                continue #continue if token matches a punctuation symbol (to approx perl script eval07)
            if goldpos not in poslist:
                continue
            if l1==l2:
                correctDep+=1.0
            scoringtokens+=1.0
        if ignorePunct:
            if scoringtokens == 0:
                la=0.0
            else:
                la = correctDep/scoringtokens *100
        else:
            la = correctDep/alltokens *100
        if b:
            print(("\t{6}\t{0:.0f}\t0\t{1:.2f}\t{2:.2f}\t{3:.0f}\t{4:.0f}\t{5:.0f} 0 0 0 0".format(alltokens,la,la,correctDep,correctDep,scoringtokens,self.idx+1)))

        return alltokens,la,la,correctDep,correctDep,scoringtokens


    def evaluateLASforPOS(self,instance,poslist=["NOUN"],ignorePunct=False,b=False):
        """
        pos-wise LAS
        """
        correctDepAndHead=0.0
        correctHead=0.0
        scoringtokens=0.0
        alltokens=0.0

        for word,wid,h1,h2,l1,l2,goldpos in zip(self.form,self.ids,self.headid,instance.headid,self.deprel,instance.deprel,instance.cpos):
            alltokens+=1
            if ignorePunct and self.isPunctuationToken(word):
                continue #continue if token matches a punctuation symbol (to approx perl script eval07)
            if goldpos not in poslist:
                continue
            if h1==h2:
                correctHead+=1.0
            if l1==l2 and h1==h2:
                correctDepAndHead+=1.0
            scoringtokens+=1.0
        if ignorePunct:
            if scoringtokens == 0:
                las=0.0
                uas=0.0
            else:
                las = correctDepAndHead/scoringtokens *100
                uas = correctHead/scoringtokens * 100
        else:
            las = correctDepAndHead/alltokens *100
            uas = correctHead/alltokens *100
        if b:
            print(("\t{6}\t{0:.0f}\t0\t{1:.2f}\t{2:.2f}\t{3:.0f}\t{4:.0f}\t{5:.0f} 0 0 0 0".format(alltokens,uas,las,correctHead,correctDepAndHead,scoringtokens,self.idx+1)))

        return alltokens,uas,las,correctHead,correctDepAndHead,scoringtokens



    def equalHeads(self,instance):
        for f1,f2 in zip(self.headid,instance.headid):
            if f1 != f2:
                return False
        return True

    def containsRelation(self,label):
        return label in self.deprel

    def containsWord(self,word):
        return word in self.form

    def equalLabels(self,instance):
        for f1,f2 in zip(self.deprel,instance.deprel):
            if f1 != f2:
                return False    
        return True

    def getHeadCPos(self,tokenid):
        if tokenid==1:
            return "-ROOT-" #since head
        head=self.headid[tokenid-1]
        return self.cpos[head-1]

    def getHeadPos(self,wid):
        if tokenid==1:
            return "-ROOT-" #since head
        head=self.headid[tokenid-1]
        return self.pos[head-1]

    def ddtPatchArticleHeadness(self):
         """ (adapted from Hector's script)
         this function changes the dependency tree of the sentence wtr to headness of articles
         #in the original Danish treebank, 
         #nouns are headed by article and the article bears the noun's role (Sb, Obj)
         #1) input example before conversion
         #1 paa 0
         #2 det 1
         #3 danske 2
         #4 hold  2
         #2) desired output
         #1 paa 0
         #2 det 4
         #3 danske 3
         #4 hold 2
         """
         for wid,form,cpos,headid in zip(self.ids,self.form,self.cpos,self.headid):
             if cpos.lower().startswith("n") and self.getHeadCPos(wid) in ["PO","PD","AC","PI"]:
                 determiner=headid
                 noun=wid
                 headcpos=self.getHeadCPos(wid)
                 #reassign head of the noun's siblings to the noun,
                 for childid,childlabel in self.getChildIds(determiner):
                     self.headid[childid-1]=noun
                     self.deprel[childid-1]=childlabel
                     
                 # set attachment
                 self.headid[noun-1]=self.headid[determiner-1]
                 self.headid[determiner-1]=noun 

                 # set label
                 if headcpos in  ["PD","PI"]:
                     self.deprel[determiner-1]="det" #determiner
                 elif headcpos in ["AC"]: # af 400 rakketter
                     self.deprel[determiner-1]="nummod" #numeric modifier
                     print("set nummod", file=sys.stderr)
                 elif headcpos in ["PO"]: # deres
                     self.deprel[determiner-1]="possd" #possessive
                 # refix incoming relation if determiner was attached to root
                 #if self.getHeadCPos(determiner) == "-ROOT-":
                 if self.headid[noun-1]==0:
                     self.deprel[noun-1]="ROOT" 
                 else:
                     print("unknown determiner label:", self.getHeadCPos(wid), file=sys.stderr)

             
    def ddtPatchPPs(self):
        """
        patches the headness of adp/pps
        in the original TB the preoposition is the head
        """
        for wid,cpos,headid in zip(self.ids,self.cpos,self.headid):
             if cpos.lower().startswith("n") and self.getHeadCPos(wid) in ["SP"]:
                 preposition=headid
                 noun=wid
                 #reassign head of the noun's siblings to the noun,
                 for childid,childlabel in self.getChildIds(preposition):
                     self.headid[childid-1]=noun
                     self.deprel[childid-1]=childlabel

                 self.headid[noun-1]=self.headid[preposition-1]
                 self.headid[preposition-1]=noun
                 self.deprel[preposition-1]="case"

                 # refix incoming relation if determiner was attached to root
                 if self.headid[noun-1]==0:
                     self.deprel[noun-1]="ROOT"



    def visualize(self):
        import networkx as nx
        import matplotlib.pyplot as plt
        G=nx.Graph()
        G.add_nodes_from([i for i in range(len(self.form))])
        edges=[(h,d) for d,h in zip(self.ids,self.headid)]
        G.add_edges_from(edges)
        #pos=nx.graphviz_layout(G,prog='dot')
        nx.draw(G)
        plt.show()

    def equalHeadsAndLabels(self,instance):
        return self.equalHeads(instance) and self.equalLabels(instance)
    
    def getSentenceLength(self):
        return len(self.form)

    def getSentence(self):
        return self.form

    def getLemmaTriples(self):
        return self.getTriples(self.lemma)

    def getFormTriples(self):
        return self.getTriples(self.form)

    def getFormUnlabeledTriples(self):
        return self.getUnlabeledTriples(self.form)

    def getTriples(self,wordform):
        triples = {}
        for i in range(len(wordform)):
            r = self.deprel[i]
            w_d = wordform[i].replace(" ","")
            hid = self.headid[i]
            if hid != 0:
                w_h = wordform[hid-1].replace(" ","")
            else:
                w_h = '<root-LEMMA>'
                
            triple = "{} {} {}".format(r,w_h,w_d)
            triples[triple] = triples.get(triple,0) + 1
        #triples = self._addExtendedTriples(triples,wordform)
        #print(triples)
        return triples

    # def _addExtendedTriples(self,triples,wordform):
    #     """ add r(A,C) for every triple r(A,B) prep(B,C) """
    #     """ e.g. comp(centinaia,di) + prep(di,feriti) => comp(centinaia,feriti) """
    #     for i in range(len(wordform)):
    #         r = self.deprel[i]
    #         w_C = wordform[i].replace(" ","")
    #         hid = self.headid[i]-1 
    #         if hid != 0:
    #             w_B = wordform[hid].replace(" ","")
    #         else:
    #             w_B = '<root-LEMMA>!'
    #         if r == "prep" and hid != 0:
    #             hid_B = self.headid[hid]-1
    #             w_A = wordform[hid_B].replace(" ","")
    #             r_new = self.deprel[hid]
    #             triple = "{} {} {}".format(r_new,w_A,w_C)
    #             triples[triple] = triples.get(triple,0) + 1

    #     #TODO
    #     """ add r(A,C) for every pair of dependecy triples r(A,B) cnj(B,C) """
    #    return triples

    def getAllLemmaTriples(self):
        return self.getAllTriples(self.lemma)

    def getAllFormTriples(self):
        return self.getAllTriples(self.form)

    def getAllTriples(self,wordform):
        """ also returns counts of parts of relation """
        triples = self.getTriples(wordform)
        actualtriples = triples.copy()
        for triple in actualtriples:
            try:
                r,w_h,w_d = triple.split(" ")

                #triple_r_w1 = "{} {} _".format(r,w_h)
                triple_r_w1 = "{} {}  ".format(r,w_h)
                triples[triple_r_w1] = triples.get(triple_r_w1,0) + 1

                # Gertjan
                #triple_w2 = "_ _ {}".format(w_d)
                #triples[triple_w2] = triples.get(triple_w2,0) + 1

                #triple_w2x = "{} _ {}".format(r,w_d)
                triple_w2x = "{}   {}".format(r,w_d)
                triples[triple_w2x] = triples.get(triple_w2x,0) + 1

                # Lin:
                #triple_r = "{} _ _".format(r)
                #triples[triple_r] = triples.get(triple_r,0) +1

            except ValueError:
                print(("Error when splitting triples: {}".format(triple)))
                sys.exit(-1)
        return triples

    def getAllUnlabeledTriples(self):
        """ also returns counts of parts of relation """
        triples = self.getFormUnlabeledTriples()
        actualtriples = triples.copy()
        for triple in actualtriples:
            try:
                r,w_h,w_d = triple.split(" ")
                r="DUMMY"

                #triple_r_w1 = "{} {} _".format(r,w_h)
                triple_r_w1 = "{} {}  ".format(r,w_h)
                triples[triple_r_w1] = triples.get(triple_r_w1,0) + 1

                # Gertjan
                #triple_w2 = "_ _ {}".format(w_d)
                #triples[triple_w2] = triples.get(triple_w2,0) + 1

                #triple_w2x = "{} _ {}".format(r,w_d)
                triple_w2x = "{}   {}".format(r,w_d)
                triples[triple_w2x] = triples.get(triple_w2x,0) + 1

                # Lin:
                #triple_r = "{} _ _".format(r)
                #triples[triple_r] = triples.get(triple_r,0) +1

            except ValueError:
                print(("Error when splitting triples: {}".format(triple)))
                sys.exit(-1)
        return triples

    def getUnlabeledTriples(self, wordform):
        triples = {}
        for i in range(len(wordform)):
            #r = self.deprel[i]
            r = "DUMMY"
            w_d = wordform[i].replace(" ","")
            p_d = self.cpos[i]
            #w_d = w_d+":"+p_d
            hid = self.headid[i]
            if hid != 0:
                w_h = wordform[hid-1].replace(" ","")
                p_h = self.cpos[hid-1]
                #w_h = w_h+":"+p_h
            else:
                w_h = '<root-LEMMA>'
                
            triple = "{} {} {}".format(r,w_h,w_d)
            triples[triple] = triples.get(triple,0) + 1
        #triples = self._addExtendedTriples(triples,wordform)
        #print(triples)
        return triples
