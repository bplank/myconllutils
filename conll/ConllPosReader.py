import sys
class ConllPosReader:
    ### read Conll POS data files (just token and POS)

    def __init__(self,filename):
        self.filename = filename
        self.startReading()

    def startReading(self):
        self.FILE = open(self.filename,"r")

    def getNext(self):
        # return next instance or None

        line = self.FILE.readline()

        line = line.strip()
        lineList = line.split("\t")

        form = []
        pos = []

        if len(lineList) == 2:
            # contains all cols, also phead/pdeprel
            while len(lineList) == 2:
                form.append(lineList[0])
                pos.append(lineList[1])

                line = self.FILE.readline()
                line = line.strip()
                lineList = line.split("\t")
        elif len(lineList) > 1:
            raise Exception("not in right format!")


        if len(form) > 0: 
            return PosInstance(form,pos)
        else: 
            return None


    def getInstances(self):
        instance = self.getNext()

        instances = []
        while instance:
            instances.append(instance)

            instance = self.getNext()
        return instances 

    def getSentences(self):
        """ return sentences as list of lists """
        instances = self.getInstances()
        sents = []
        for i in instances:
            sents.append(i.form)
        return sents

   


class PosInstance:
    
    def __init__(self, form,pos):
        self.form = form
        self.pos = pos
        
    def __str__(self):
        s = "{0}\t{1}\n"
        sout = ""
        for i in range(len(self.form)):
            sout += s.format(self.form[i],self.pos[i])
        return sout

    def __repr__(self):
        return self.__str__()

    def equalForm(self,instance):
        for f1,f2 in zip(self.form,instance.form):
            if f1 != f2:
                return False
        return True
    

    def containsWord(self,word):
        return word in self.form

    def getSentenceLength(self):
        return len(self.form)

    def getSentence(self):
        return self.form

