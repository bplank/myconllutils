import sys
class SentenceReader:
    ### read plain sentence file, one per line

    def __init__(self,filename):
        self.filename = filename
        self.startReading()

    def startReading(self):
        self.FILE = open(self.filename,"r")

    def getNext(self):
        # return next instance or None

        line = self.FILE.readline()
        line = line.strip()
        if line:
            return Sentence(line)
        else:
            return None

    def getInstances(self):
        instance = self.getNext()

        instances = []
        while instance:
            instances.append(instance)

            instance = self.getNext()
        return instances 

    def getSentences(self):
        """ return sentences as list of lists """
        instances = self.getInstances()
        sents = []
        for i in instances:
            sents.append(i.form)
        return sents

   


class Sentence:
    
    def __init__(self,sentence):
        self.sentence = sentence
        
    def __str__(self):
        return self.sentence

    def __repr__(self):
        return self.__str__()

    def getSentenceLength(self):
        return len(self.sentence.split())

    def getSentence(self):
        return self.sentence


