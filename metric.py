### Adapted script from Vincent van Asch, cf.
### http://www.clips.uantwerpen.be/~vincent/thesis-software/metric.py

### Changes: rename "overlap" to "OOV"

from math import sqrt, log

UNDEF = 'undefined'


#from metrics.kendall import tau_a, tau_b

def compute(p, q, verbose=False):
    '''
    in general:
    p: test
    q: train
    
    Computes:
    
    - Euclidean
    - Variational
    - Kullback - Leibler
    - Jensen - Shannon
    - Skew
    - Renyi
    - Cosine
    - Bhattacharyya distance1
    - Unknown words (overlap) = overlap (in thesis)  == OOV
    - Rev Unknown Words (rev overlap) = sUWR
    '''
    
    euclidean=0
    variational=0
    kl=0
    js=0
    skewalpha =[0.01, 0.05, 0.1, 0.25, 0.5, 0.75, 0.90, 0.95, 0.99]
    skew = [0 for i in range(len(skewalpha))]
    #renyi_alpha = [0.95, 0.99, 1.05, 1.1]
    #renyi_alpha = [0.95, 0.99]
    renyi_alpha=[0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 0.99, 1.05, 1.1, 1.5, 2, 3]
    renyi=[0 for i in range(len(renyi_alpha))]
    cosine =[0,0,0]
    bhat1 = 0
    overlap = [0,0] # Unknown Words
    revoverlap = [0,0]
    
    for k in set(p.keys()).union(set(q.keys())):
        # Data
        pk = p[k]
        qk = q[k]
    
        
        # Euclidean
        euclidean += pow(pk-qk, 2)
        
        # Variational
        variational += abs(pk - qk)
        
        # (smoothed) Kullback-Leibler
        if pk != 0:            
            if qk == 0:
                if verbose:
                    print '%s has frequency 0 in distribution q. Approximating with pow(2, -52)' %str(k)
                qk = pow(2, -52)
            
            kl += pk*log(pk/qk, 2)
            
            
            if qk == pow(2, -52):
                qk = 0
            
        # Skew
        for i, alpha in enumerate(skewalpha):
            s = alpha*pk + (1-alpha)*qk
            
            if qk == 0:
                qlog = 0
            else:
                qlog = log(qk/s, 2)
            skew[i] = skew[i] + qk*qlog
            
            
        # Jensen-Shannon
        avg = (pk+qk)/2.0
        
        if pk != 0:
            plog = log(pk/avg, 2)
        else:
            plog=0
        if qk != 0:
            qlog = log(qk/avg, 2)
        else:
            qlog = 0
        js += 0.5*( pk*plog + qk*qlog )
            
        
            
        # Renyi
        if qk != 0:
            for i,alpha in enumerate(renyi_alpha):
                renyi[i] = renyi[i] + (pow(pk, alpha)*pow(qk, 1.0-alpha))
        
 
        # Cosine
        cosine[0] = cosine[0] + pk*qk
        cosine[1] = cosine[1] + pk*pk
        cosine[2] = cosine[2] + qk*qk
        
        # Bhattacharyya distance
        bhat1 += sqrt(pk*qk)
        
        
        # overlap
        if qk != 0:
            if pk != 0:
                overlap[0]+=1
            overlap[1]+=1
           
        # reverse overlap
        if pk != 0:
            if qk != 0:
                revoverlap[0]+=1
            revoverlap[1]+=1
    
        

    # Euclidean
    euclidean = sqrt(euclidean)
       
    # Renyi
    for i,alpha in enumerate(renyi_alpha):
        try:
            renyi[i] = 1/(1.0-alpha) * log(renyi[i], 2)
        except ValueError:
            print 'alpha:', alpha
            print 'Renyi:', renyi[i]
            renyi[i] = UNDEF
            #raise

    # Cosine
    try:
        cosine = 1 - cosine[0]/sqrt(cosine[1]*cosine[2])
    except ZeroDivisionError:
        print 'numerator', cosine[0]
        print 'denominator', cosine[1], cosine[2]
        cosine = UNDEF
        #raise
    
    # Bhattacharyya distance
    try:
        bhat = sqrt(1.0-bhat1)
    except ValueError:
        bhat= UNDEF
    #bhat1 = -1*log(bhat1)
    
    # Overlap
    try:
        overlap = 1 - (float(overlap[0])/overlap[1])
    except ZeroDivisionError:
        overlap = UNDEF
    # Reverse Overlap
    try:
        revoverlap = 1 - (float(revoverlap[0])/revoverlap[1])
    except ZeroDivisionError:
        revoverlap = UNDEF
    
    
    output={ 'Euclidean':euclidean, \
             'Variational (L1)':variational, \
             'Kullback-Leibler':kl, \
             'Bhattacharyya':bhat, \
             'Cosine':cosine, \
             #'Overlap': overlap, \
             'OOV': overlap, \
             'sUWR': revoverlap, \
             'Jensen-Shannon':js
             }
             #'Tau a': tau_a(p,q), \
             #'Tau b': tau_b(p,q)}
             
    #output = { 'Overlap': overlap, \
    #           'Reverse overlap': revoverlap }
         
    #output={'Bhattacharyya':bhat}

    
    for i,alpha in enumerate(skewalpha):
        output['Skew_with_alpha=%4.3f' %alpha] = skew[i]
    
    
    for i,alpha in enumerate(renyi_alpha):
        #output['R\xc3\xa9nyi_with_alpha=%4.3f' %alpha] = renyi[i]
        output['Renyi_with_alpha=%4.3f' %alpha] = renyi[i]
    
        
            
    return output
    
    
    
