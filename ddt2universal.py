#!/usr/bin/python

""" 
Author: Barbara Plank
Date:   April 2015

Description: This script applies patches to the CDT/DDT (CoNLL) to convert it to universal dependencies


"""
import sys
from conll.Conll07Reader import Conll07Reader
from optparse import OptionParser

def convert2universal(instance):
    """
    convert ddt/cdt to universal

    - nppatch-v1: in the original TB, nouns are headed by determiners; reattach so that noun is head

    """
    instance.ddtPatchArticleHeadness()
    instance.ddtPatchPPs()
    return instance
    
def main():
    usage = "usage: %prog [options] ddtfile"

    parser = OptionParser(usage=usage)

#    parser.add_option("-c", dest="c",default=False,action="store_true",help="complete predicates (SemEval-15)")
 
    (options, args) = parser.parse_args()

    if len(args) < 1:
        print("Argument missing!")
        parser.print_help()
        exit(-1)
    
    fsys = args[0]
 
    reader1 = Conll07Reader(fsys)
    instances = reader1.getInstances()
    for instance in instances:
        if instance.getSentenceLength() < 5:
            print convert2universal(instance)



if __name__=="__main__":
    main()
