#!/usr/bin/python

""" 
Author: Barbara Plank
Date:   August 2011

Description: Sort parses by sentence length

Options: specify minimun and maximum sentence length to extract

"""
import sys
from conll.Conll07Reader import Conll07Reader
from optparse import OptionParser

def main():
    usage = "usage: %prog [options] FILE"

    parser = OptionParser(usage=usage)
    parser.add_option("--gold", dest="gold",
                  help="extract also gold data for diff samples")
    parser.add_option("--minLen", dest="minLen",default=0,type="int",
                  help="extract sentences that have a minimum length")
    parser.add_option("--maxLen", dest="maxLen",type="int",
                  help="extract sentences up to this length")
    

    (options, args) = parser.parse_args()

    if len(args) < 1:
        print("Argument missing!")
        parser.print_help()
        exit(-1)
    
    file1 = args[0]
    
    reader1 = Conll07Reader(file1)

    instances1 = reader1.getInstances()
    selectedInstances=[]
    for i in instances1:
        if i.getSentenceLength() > options.minLen:
            if options.maxLen:
                if i.getSentenceLength() <= options.maxLen:
                    selectedInstances.append((i.getSentenceLength(),i.getSentence()[0],i))
            else:
                selectedInstances.append((i.getSentenceLength(),i.getSentence()[0],i))

    for length,tok1,instance in sorted(selectedInstances):
        print instance

    if options.gold:
        readerG = Conll07Reader(options.gold)
        instancesG=readerG.getInstances()
        for length,tok1,i in sorted(selectedInstances):
            print >>sys.stderr,instancesG[i.getIdx()]

main()
