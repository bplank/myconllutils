#!/usr/bin/env python
import codecs
import sys
import operator

def main():
    if len(sys.argv) < 3:
        print("./overlap.py SRC TRG")
        exit()
    
    SRC = codecs.open(sys.argv[1],encoding="utf-8")
    TRG = codecs.open(sys.argv[2],encoding="utf-8")

    vocabSRC={}
    vocabTRG={}

    for line in SRC:
        line = line.strip()
        if line:
            t= line.split("\t")[1]
            vocabSRC[t] = vocabSRC.get(t,0) +1
    SRC.close()

    for line in TRG:
        line = line.strip()
        if line:
            t=line.split("\t")[1]
            vocabTRG[t] = vocabTRG.get(t,0) +1
    TRG.close()

    #count how many tokens in trg have been observed
    observed=0
    notObserved=0
    onlyTRG = {}
    matching = {}
    for token in vocabTRG.keys():
        if vocabSRC.get(token):
            observed+=1
            matching[token] = matching.get(token)
        else:
            notObserved+=1
            onlyTRG[token] = vocabTRG.get(token)

    print("observed:",observed)
    print("notObserved:",notObserved)
    print("vocabTrg size:", len(vocabTRG))
    #print("vocabSRCsize:", len(vocabSRC))

    print("ratio observed:", observed/float((len(vocabTRG))))
    print("OOV_types:", notObserved/float((len(vocabTRG)))*100)
    #print("OOV:", notObserved/float((len(vocabSRC)))*100)

    TRG = codecs.open(sys.argv[2],encoding="utf-8")
    oov_token_count=0.0
    oov_token_total=0.0
    for line in TRG:
        line = line.strip()
        if line:
            t=line.split("\t")[1]
            if t not in vocabSRC:
                oov_token_count+=1
            oov_token_total+=1
    TRG.close()

    print("OOV_tokens:", oov_token_count/oov_token_total*100)

    #per-sentence OOV
    oov_token_count=0.0
    oov_token_total=0.0
    TRG = codecs.open(sys.argv[2],encoding="utf-8")
    i=0
    sys.stderr.write("OOV_token_mean_sent\n")
    for line in TRG:
        line = line.strip()
        if line:
            t=line.split("\t")[1]
            if t not in vocabSRC:
                oov_token_count+=1
            oov_token_total+=1
        else:
            sys.stderr.write("{1}\n".format(i, oov_token_count/oov_token_total))            
            oov_token_count=0.0
            oov_token_total=0.0
            i+=1
    TRG.close()





    mostFreq = sorted(onlyTRG.items(), key=lambda x: x[1],reverse=True)
    print(mostFreq[:20])
    
    print(len(onlyTRG))
    print(len(matching))
    print(len(vocabSRC))
    #print matching words to syerr
    #for t in matching:
    #    #print(t,file=sys.stderr)
    #    print(t)

    

if __name__=="__main__":
    main()
