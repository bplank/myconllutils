#!/usr/bin/python

""" 
Author: Barbara Plank
Date:   August 2011

Description: exclude sentences by sentence id



"""
import sys
from conll.Conll07Reader import Conll07Reader
from optparse import OptionParser

def main():
    usage = "usage: %prog [options] FILE"

    parser = OptionParser(usage=usage)
    parser.add_option("--first", dest="first",default=200,type="int")    
    parser.add_option("--exclude", dest="exclude",type="str",help="list of ids separated by comma, e.g. 0,10")    
    #exclude=[0,1,2,3,45,47,87] 

    (options, args) = parser.parse_args()
    exclude = map(int, options.exclude.split(","))

    if len(args) < 1:
        print("Argument missing!")
        parser.print_help()
        exit(-1)
    
    file1 = args[0]
    
    reader1 = Conll07Reader(file1)

    instances1 = reader1.getInstances()

    for idx, instance in enumerate(instances1[:options.first]):
        if idx in exclude:
            continue
        else:
            print instance

main()
