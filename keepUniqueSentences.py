import argparse
import sys
from collections import Counter


parser = argparse.ArgumentParser(description="Given two CONLL06 files, prints outs the sentences in FILE1 that are not in FILE2 ")
parser.add_argument("file1",   metavar="FILE", help="name of the parsedfile file")
parser.add_argument("file2", metavar="FILE", help="name of the parsedfile file")
args = parser.parse_args()


file2_sentences = set()
acc = []
for line in open(args.file2).readlines():
    if len(line) < 3:
        file2_sentences.add(" ".join(acc))
        acc = []
    else:
        acc.append(line.strip().split("\t")[1])
file2_sentences.add(" ".join(acc))


OUT = 0
acc = []
current_sentence = []
for line in open(args.file1).readlines():
    if len(line) < 3:
        if " ".join(acc) not in file2_sentences:
            for l in current_sentence:
                print l
            print
        else:
            OUT+=1
        acc = []
        current_sentence = []
    else:
        acc.append(line.strip().split("\t")[1])
        current_sentence.append(line.strip())

if " ".join(acc) not in file2_sentences:
    for l in current_sentence:
        print l
    print
else:
    OUT+=1

#print OUT