#!/usr/bin/python3
# -*- coding: utf-8 -*-
""" 
Author: Barbara Plank
Date:   Jan 2015

remove id=NUM from feature column in danish treebank


"""
import sys
from conll.Conll07Reader import Conll07Reader
from optparse import OptionParser

def main():
    usage = "usage: %prog [options] FILE"

    parser = OptionParser(usage=usage)
    parser.add_option("-f", "--removeIdFeat",
                      dest="removeFeat", help="remove feature column id feature (eg. id=11)",default=True,action="store_true")

    parser.add_option("-a", "--all",
                      dest="all", help="remove all features",default=True,action="store_true")

    (options, args) = parser.parse_args()
     
    if len(args)>=1:
        instances=[]
        for file1 in args:
        # read file
            reader1 = Conll07Reader(file1)
            instances1 = reader1.getInstances()

            for instance in instances1:
                instance.removeIdFeat(all=options.all)
                print(instance)
    else:
        reader1 = Conll07Reader(sys.stdin,stdin=True)
        instances1 = reader1.getInstances()

        for instance in instances1:
            instance.removeIdFeat(all=options.all)
            print(instance)


if __name__=="__main__":
    main()
