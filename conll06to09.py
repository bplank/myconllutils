#!/usr/bin/python3

""" 
Author: Barbara Plank
Date:   Oct 2014

Convert Conll06/07 file to Conll09


"""
import sys
from conll.Conll07Reader import Conll07Reader
from optparse import OptionParser

def main():
    usage = "usage: %prog [options] FILE"

    parser = OptionParser(usage=usage)
    parser.add_option("-c", "--poscolumnindex",
                      dest="poscolidx", help="poscolidx to use from Conll07 to Conll09, default=3 (cpos)",type=int,default=3)
    parser.add_option("-f", "--removeFeatCol",
                      dest="removeFeats", help="remove feature column info (for crosslingual parsing)",default=False,action="store_true")
    parser.add_option("-w", "--removeWord",
                      dest="removeWord", help="remove word form column info (for crosslingual parsing)",default=False,action="store_true")
    parser.add_option("-l", "--removeLemma",
                      dest="removeLemma", help="remove lemma form column info (for crosslingual parsing)",default=False,action="store_true")

    (options, args) = parser.parse_args()
     
    #if len(args) < 1:
    #    print("Argument missing!")
    #    parser.print_help()
    #    exit(-1)

    if len(args)>=1:
        instances=[]
        for file1 in args:
        # read file
            reader1 = Conll07Reader(file1)
            instances1 = reader1.getInstances()

            for instance in instances1:
                print(instance.printConll09(poscolidx=options.poscolidx,removeFeats=options.removeFeats,removeWord=options.removeWord,removeLemma=options.removeLemma))
    else:
        reader1 = Conll07Reader(sys.stdin,stdin=True)
        instances1 = reader1.getInstances()

        for instance in instances1:
            print(instance.printConll09(poscolidx=options.poscolidx,removeFeats=options.removeFeats,removeWord=options.removeWord,removeLemma=options.removeLemma))


if __name__=="__main__":
    main()
