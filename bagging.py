#!/usr/bin/python

"""
Author: Barbara Plank
Date:   August 2012

Description: this script create a bootstrap sample of a given
CoNLL file (=bagging)

It randomly samples N instances from a training set of size N with replacement
(Breiman, 1994)

"""
import sys
import random
import math
import numpy
from conll.Conll07Reader import Conll07Reader
from conll.ConllPosReader import ConllPosReader
from conll.SentenceReader import SentenceReader
from optparse import OptionParser

def main():
    usage = "usage: %prog [options] FILE"

    parser = OptionParser(usage=usage)
    parser.add_option("-f", "--format",
                      dest="format", help="input format [dep|pos|sent] (default:dep) ",default="dep")

    parser.add_option("-s", "--size",
                      dest="size", help="size of bootstrap sample. if not specified, same as input file",type=int)
    parser.add_option("-p", "--percent",
                      dest="percent", help="percent of bootstrap sample. if not specified, 100% as input file. Incompatible with -s",type=float)

    parser.add_option("-x", "--seed",dest="seed",default=None,help="seed for sampler",type=int)
    parser.add_option("-e", "--exponential", dest="exponential",default=None,help="first draw from exponential parametrized by lambda number of domains (use lambda=0.4 like McClosky)",type=float)
    parser.add_option("-u", "--uniform", dest="uniform",help="draw number of domains from uniform distribution",action="store_true",default=False)

    parser.add_option("-d", "--diff", dest="diff",help="draw same instances from 2 files uniform at random WITHOUT replacement",action="store_true",default=False)

    (options, args) = parser.parse_args()

    if len(args) < 1:
        print("Argument missing!")
        parser.print_help()
        exit(-1)

    if options.seed:
        random.seed(options.seed)

    infiles=[]
    if options.exponential:
        numDom=int(math.ceil(numpy.random.exponential(1/options.exponential,1)))
        if numDom>len(args):
            numDom=len(args)
        print >>sys.stderr, "args:", args
        infiles.extend(random.sample(args,numDom)) #without replacement
        print >>sys.stderr, "infiles", infiles
        print >>sys.stderr, "numDom:", numDom
    elif options.uniform:
        numDom=random.randint(1,len(args))
        infiles.extend(random.sample(args,numDom)) #without replacement
        print >>sys.stderr, "args:", args
        print >>sys.stderr, "infiles", infiles
        print >>sys.stderr, "numDom:", numDom

    else:
        # use all
        infiles=args

    
    instances=[]
    filenames=[]
    if not options.diff:
        for file1 in infiles:
        # read file
            if options.format == "dep":
                reader1 = Conll07Reader(file1)
            elif options.format == "pos":
                reader1 = ConllPosReader(file1)
            elif options.format == "sent":
                reader1=SentenceReader(file1)
            else:
                print("unknown input format!")
                exit()
            instances1 = reader1.getInstances()
            instances.extend(instances1)
            filenames.extend([file1 for i in range(0,len(instances1))])
    else:
        # assumes to keep files in parallel ##hacky now for just 2 files
        instances=Conll07Reader(infiles[0]).getInstances()
        instances2=Conll07Reader(infiles[1]).getInstances()
        

    samplesize=len(instances)
    if options.size:
        samplesize=options.size
    elif options.percent:
        samplesize=int(float(samplesize)*float(options.percent)/100.0)

    if options.diff:
        #without replacement
        assert(len(instances)==len(instances2))
        indices=list(range(0,samplesize))
        sampled_idx=random.sample(indices,options.size)#sample without repl.
        for i in sampled_idx:
            print instances[i]
            print >>sys.stderr, instances2[i]
    else:
        #with replacement
        for i in range(samplesize):
            idx_sel = random.randint(0,len(instances)-1) #randint is inclusive
            instance = instances[idx_sel]
        #instance = random.choice(instances)
        print(instance)
        print >>sys.stderr, filenames[idx_sel]

if __name__=="__main__":
    main()
