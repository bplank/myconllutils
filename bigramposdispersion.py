import argparse
from collections import Counter
import numpy as np
from scipy import stats
from numpy import log2, sum, isnan

def main():
    parser = argparse.ArgumentParser(description="Calculates divergences between the pos bigrams of two conll files; prints out 1) the pearson coefficient and 2) its significante, 3) the JS and the 4) KL divergence")

    parser.add_argument("-s", "--source", required=True, metavar="FILE", help="name of the parsedfile file")
    parser.add_argument("-t", "--target", required=True, metavar="FILE", help="name of the parsedfile file")

    args = parser.parse_args()
    
    metrics = compute(args.source, args.target)
    #print args.source+"\t"+args.target+"\t"+pear+"\t"+str(JS(distsource,disttarget))+"\t"+str(KL(distsource,disttarget))    
    print(args.source+"\t"+args.target+"\t"+metrics["pos2-pear"]+"\t"+str(metrics["pos2-JS"])+"\t"+str(metrics["pos2-KL"]))    


def compute(sourcefile,targetfile):
    ### compute pos bigram divergences 

    spos = getPos(sourcefile)
    tpos = getPos(targetfile)

    postags = set(spos).union(set(tpos))
    bigrams = []
    for x in postags:
        for y in postags:
            bigrams.append((x+"-"+y))

    countsource = GetPosBigramsCounts(spos)
    counttarget = GetPosBigramsCounts(tpos)
    distsource = []
    disttarget = []
    for b in bigrams:
        distsource.append(1+(countsource[b]*1.0))
        disttarget.append(1+(counttarget[b]*1.0))

    disttarget = np.array(disttarget)
    distsource = np.array(distsource)
    disttarget = disttarget / sum(disttarget)
    distsource = distsource / sum(distsource)

    pear = stats.pearsonr(distsource,disttarget)
    pear = str(pear[0]) + "\t"+ str(pear[1])

    kl=KL(distsource,disttarget)
    js=JS(distsource,disttarget)

    output={ 'pos2-pear':pear, \
             'pos2-KL':kl, \
             'pos2-JS':js , \
             }
    return output


def KL (x, y):
    d1 = x*log2(2*x/(x+y))
    d1[isnan(d1)] = 0
    d = 0.5*sum(d1)
    return d


def JS(x,y): #Jensen-shannon divergence
    import warnings
    warnings.filterwarnings("ignore", category = RuntimeWarning)
    d1 = x*log2(2*x/(x+y))
    d2 = y*log2(2*y/(x+y))
    d1[isnan(d1)] = 0
    d2[isnan(d2)] = 0
    d = 0.5*sum(d1+d2)
    return d


def GetPosBigramsCounts(pos):
    a = []
    for x, y in zip(pos, pos[1:]):
        a.append(x+"-"+y)
    return Counter(a)
def getPos(file):
    a = []
    for line in open(file).readlines():
     if len(line) < 2:
         pass
     else:
        a.append(line.strip().split("\t")[1])
    return a




if __name__=="__main__":
    main()


