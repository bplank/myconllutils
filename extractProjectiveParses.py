
"""
Author: Hector Martinez
Date:   June 2014

Description: Given a CoNLL 2007 files, this script extracts the sentences that are fully projective.


"""

import argparse

from conll.Conll07Reader import Conll07Reader

parser = argparse.ArgumentParser(description="Extracts relations from syntactic- or dependency-parsed input")
parser.add_argument("infile",   metavar="FILE", help="name of the parsedfile file")
args = parser.parse_args()

def ContainsNonProjectivity(tree):
    countProjectiveRelation=0.0
    countNonProjectiveRelation=0.0
    isProjective = True
    instance = dict((id, head) for id,head in zip(tree.ids, tree.headid))
    for edge in instance:
            i = instance[edge]
            j = edge

            if j < i:
                i,j=j,i
            for k in range(i+1,j):
                headk = instance[k]
                if i <= headk <= j or j <= headk <= i:
                    projEdge = True
                    countProjectiveRelation+=1
                else:
                    #print("non-projective")
                    #print("{} <= {} <= {} ? ".format(i,headk,j))
                    isProjective=False
                    countNonProjectiveRelation+=1
    return isProjective





def main():
    reader1 = Conll07Reader(args.infile)

    instances1 = reader1.getInstances()

    for i in instances1:
        if ContainsNonProjectivity(i):
            print i




main()
#
#
#
#
# import sys
#
# if len(sys.argv) != 2:
#     print("Error: specify a CoNLL file!\nUsage: {0} {1}".format(sys.argv[0],"FILE"))
# else:
#     filename = sys.argv[1]
#     FILE = open(filename,"r")
#
#     instance = {}
#     wordid=1
#     instances = []
#
#     while True:
#         l = FILE.readline()
#         if not l:
#             break
#
#         l = l.strip()
#         lineList = l.split("\t")
#
#         if len(lineList) > 1:
#             instance[wordid] = int(lineList[6])
#             wordid = wordid+1
#         else:
#             instances.append(instance)
#             instance = {}
#             wordid=1
#
#
#     countProjective=0.0
#     countNonProjective=0.0
#
#     countProjectiveRelation=0.0
#     countNonProjectiveRelation=0.0
#
#     for instance in instances:
#         isProjective=True
#         pos=0.0
#         wordid=pos+1
#         #print("instance: {}".format(instance))
#
#         for edge in instance:
#             i = instance[edge]
#             j = edge
#
#             if j < i:
#                 i,j=j,i
#             for k in range(i+1,j):
#                 headk = instance[k]
#                 if i <= headk <= j or j <= headk <= i:
#                     projEdge = True
#                     countProjectiveRelation+=1
#                 else:
#                     #print("non-projective")
#                     #print("{} <= {} <= {} ? ".format(i,headk,j))
#                     isProjective=False
#                     countNonProjectiveRelation+=1
#
#
#         if isProjective:
#             countProjective+=1
#         else:
#             countNonProjective+=1