#!/usr/bin/python

""" 
Author: Hector Martinez
Date:   February 2015

Description: Extract N random sentences between minLen and maxLen tokens in length

"""
import sys
from conll.Conll07Reader import Conll07Reader
import argparse
import random

def main():
    usage = "usage: %prog [options] FILE"

    parser = argparse.ArgumentParser(description="Extracts N random sentences with lengths between min, max")
    parser.add_argument("file1")
    parser.add_argument("--N", default=20,type=int)
    parser.add_argument("--minLen",default=8,type=int,help="extract sentences that have a minimum length")
    parser.add_argument("--maxLen", default=20,type=int, help="extract sentences up to this length")
    parser.add_argument("--seed", default=111,type=int,help="random seed")


    args = parser.parse_args()

    reader1 = Conll07Reader(args.file1)
    instances1 = reader1.getInstances()
    selectedInstances=[]

    for i in instances1:
        if i.getSentenceLength() > args.minLen:
                if (i.getSentenceLength() - args.maxLen) < 0:
                    #print i.getSentenceLength()
                    selectedInstances.append(i)

    print(len(selectedInstances), len(instances1))
    random.seed(args.seed)
    random.shuffle(selectedInstances)
    for i in selectedInstances[:args.N]:
        print(i)

    for i in selectedInstances[:args.N]:
        print(" ".join(i.getSentence()), file=sys.stderr)

main()
