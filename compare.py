#### script to get statistics from two conll-style POS files
import sys
import distribution
import metric
import argparse
import bigramposdispersion
import json
import numpy

parser = argparse.ArgumentParser(description="Calculates several metrics between two conll files")

parser.add_argument("-s", "--source", required=True, help="name of source file")
parser.add_argument("-t", "--target", required=True, help="name of target file")
parser.add_argument("-a", "--accuracy", required=False, help="accuracy on target file(s)")
parser.add_argument("-d", "--description", required=False, help="description (e.g. head of trace file)")
parser.add_argument("-w", "--stopwords", required=False, help="name of stopword file (to calculate metrics on closed class items = in stopword list)")
args = parser.parse_args()



tokenfield=0
sd=distribution.getdistributionfromcolumn(args.source, field=tokenfield, sep="\t", marker=None)
td=distribution.getdistributionfromcolumn(args.target, field=tokenfield, sep="\t", marker=None)

#print sd
print >>sys.stderr, "====>Token-based divergence measures"
metrics=metric.compute(sd,td)
for m in metrics:
    print >>sys.stderr,  m, metrics[m]

### pos bigram metrics (from Hector's script)
print >>sys.stderr, "====>POS-bigram-based divergence measures"
metricspos2=bigramposdispersion.compute(args.source,args.target) #take in original conll files

for m in metricspos2:
    print >>sys.stderr,  m, metricspos2[m]

### metrics on closed-word items
if args.stopwords:
    sdCC=distribution.getdistributiononclosedclassitemsfromcolumn(args.source, args.stopwords, field=tokenfield, sep="\t", marker=None)
    tdCC=distribution.getdistributiononclosedclassitemsfromcolumn(args.target, args.stopwords, field=tokenfield, sep="\t", marker=None)

    print >>sys.stderr, "====>divergence on closed-class items"
    metricsCC=metric.compute(sdCC,tdCC)
    for m in metricsCC:
        print >>sys.stderr,  m, metricsCC[m]
        

    #open class metrics
    sdOC=distribution.getdistributiononclosedclassitemsfromcolumn(args.source, args.stopwords, field=tokenfield, sep="\t", marker=None,invert=True)
    tdOC=distribution.getdistributiononclosedclassitemsfromcolumn(args.target, args.stopwords, field=tokenfield, sep="\t", marker=None,invert=True)

    print >>sys.stderr, "====>divergence on open-class items"
    metricsOC=metric.compute(sdOC,tdOC)
    for m in metricsOC:
        print >>sys.stderr,  m, metricsOC[m]
        
    results = { "src": args.source, \
                    "trg": args.target, \
                    "tok": metrics, "pos": metricspos2, "CC": metricsCC, "OC":metricsOC} 

else:
    results = { "src": args.source, \
                "trg": args.target, \
                "tok": metrics, "pos": metricspos2}

if args.accuracy:
    if len(args.accuracy.split())>1:
        acc=numpy.array([float(x) for x in args.accuracy.split()])
        results["accuracy_all"] = args.accuracy
        mean = acc.mean()
        std=acc.std()
        results["accuracy"] = mean
        results["accuracy_std"] = std
    else:
        results["accuracy"] = args.accuracy
if args.description:
    results["description"] = json.dumps(args.description)

print json.dumps(results)
