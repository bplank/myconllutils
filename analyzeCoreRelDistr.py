#!/usr/bin/python

""" 
Author: Barbara Plank
Date:   August 2011

Description: This script gets two CoNLL parse file and extracts parses that differ

Options: specify minimun and maximum sentence length to extract

"""
import os
import sys
from scipy import stats
from conll.Conll07Reader import Conll07Reader
from optparse import OptionParser
from collections import Counter,defaultdict
import numpy as np
import glob
from bigramposdispersion import KL, JS
def main():
    usage = "usage: %prog [options] FOLDER"

    parser = OptionParser(usage=usage)
    (options, args) = parser.parse_args()

    if len(args) < 1:
        print("Argument missing!")
        parser.print_help()
        exit(-1)

    distr_core = {}

    languages = []
    for lfile in glob.glob(args[0]+"/*.lex"):
        lang = os.path.basename(lfile).split("-")[0]
        print(lang, lfile)
        languages.append(lang)

        readerGold=Conll07Reader(lfile)
        instancesGold=readerGold.getInstances()

        result = defaultdict(int)
        
        for instance in instancesGold:
            for what in ["core","func","non_core","punct"]:
                rels, total_rels, arr_cores = instance.getCoreDeprelCount(what=what)
                result['total_%s' % what] += rels
                if what == "core": # just count once
                    result['total'] += total_rels
                    if not 'core_distr' in result:
                        result['core_distr'] = []
                    result['core_distr'].extend(arr_cores)
        for what in ["core","func","non_core","punct"]:
            print("total_%s %s total: %s percent: %s" % (what, result[('total_%s' % what)], result['total'], result[('total_%s' % what)]/result['total']))
        counter = Counter(result['core_distr'])
        total = np.sum([counter[key] for key in counter.keys()])
        print(total)
        print(counter)
        counter_norm = {key: value/float(total) for key,value in counter.items()}
        print(counter_norm)
        distr_core[lang] = counter
        #print(Counter(result['core_distr']))

    similarity_core = {}
    ### get pairs of languages KL div 
    print(compute(distr_core['da'],distr_core['en']))
    for lang_i in languages:
        for lang_j in languages:
            similarity_core["%s-%s" % (lang_i,lang_j)] = compute(distr_core[lang_i],distr_core[lang_j])
            marker=""
            if float(similarity_core["%s-%s" % (lang_i,lang_j)]['pos2-pear'].split()[1]) > 0.05:
                marker = "***"
            print("%s-%s" % (lang_i,lang_j), similarity_core["%s-%s" % (lang_i,lang_j)]['pos2-pear'], marker)
#    print(similarity_core['da-es'])
#    print(similarity_core['es-da'])
#    print(similarity_core['da-da'])

def compute(source, target):
    ### compute divergences over counts in source and target

    labels = set(source.keys()).union(set(target.keys()))

    distsource = []
    disttarget = []
    for b in labels:
        distsource.append(1+(source[b]*1.0))
        disttarget.append(1+(target[b]*1.0))

    disttarget = np.array(disttarget)
    distsource = np.array(distsource)
    disttarget = disttarget / sum(disttarget)
    distsource = distsource / sum(distsource)

    pear = stats.pearsonr(distsource,disttarget)
    pear = str(pear[0]) + "\t"+ str(pear[1])

    kl=KL(distsource,disttarget)
    js=JS(distsource,disttarget)

    output={ 'pos2-pear':pear, \
             'pos2-KL':kl, \
             'pos2-JS':js , \
             }
    return output

main()
