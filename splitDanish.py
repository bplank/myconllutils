#!/usr/bin/python3

from optparse import OptionParser
import sys
import os
from conll.Conll07Reader import Conll07Reader

""" 
Author: Barbara Plank
Date:   May 2015

Description: Split DDT data (train and dev) into single files

"""

def splitData(filename,destination):
    d = os.path.dirname(filename)
    base = os.path.basename(filename).replace(".conll","")
    d=d+"/"+destination
    print d
    if not os.path.exists(d):
        os.makedirs(d)
        os.makedirs(d+"/orig")
        os.makedirs(d+"/target")
    reader=Conll07Reader(filename)
    instances=reader.getInstances()
    
    countMultiRoot=0
    countTotal=0
    LOG=open(d+"/"+base+".log","w")
    for i,instance in enumerate(instances):
        dir=d+"/orig/"+base
        multiRoot=instance.reattachRoot()
        if multiRoot:
            countMultiRoot+=1
        LOG.write("{} multiRoot={}\n".format(i,multiRoot))
        countTotal+=1
        writeOut(i,instance,dir,multiRoot)
    LOG.close()
    return countTotal,countMultiRoot

def writeOut(i,instance,dir,multiRoot):
    i=str(i).zfill(5)
    ORIG=open(dir+"."+i+".conll","w")
    
    s = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\n"
    sout = ""
    for i in range(len(instance.form)):
        sout += s.format(instance.ids[i],instance.form[i],instance.lemma[i],instance.cpos[i],instance.pos[i],instance.feats[i],instance.headid[i],instance.deprel[i],instance.phead[i],instance.pdeprel[i])
    sout+="\n"
    ORIG.write(sout)
    ORIG.close()

        
def main():    
    usage = "usage: %prog [options] FILE"

    parser = OptionParser(usage=usage)
    parser.add_option("-d", "--destination", dest="destination",help="name of destination folder")

    (options, args) = parser.parse_args()

    print args
    if len(args)>= 1:
        
        for filename in args:
            total,multiroot=splitData(filename,options.destination)
            print total,multiroot
    else:
        print("Please provide a CoNLL file!")
        parser.print_help()
        sys.exit(-1)


if __name__=="__main__":

    main()
