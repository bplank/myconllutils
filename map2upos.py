#!/usr/bin/python

""" 
Author: Hector Martinez
Date:   February 2015

Description: Extract N random sentences between minLen and maxLen tokens in length

"""
import sys
from conll.Conll07Reader import Conll07Reader
import argparse
import random

def main():
    usage = "usage: %prog [options] FILE"

    parser = argparse.ArgumentParser(description="Extracts N random sentences with lengths between min, max")
    parser.add_argument("file1")
    parser.add_argument("--mapFile")

    args = parser.parse_args()

    mapping = {}
    for x in open(args.mapFile, 'rU'):
        x = x.strip()
        try:
            k, v = x.split('\t')
            mapping[k] = v
        except ValueError:
            sys.stderr.write('\tignoring line "%s": wrong format\n' % x)
            pass


    reader1 = Conll07Reader(args.file1)
    instances1 = reader1.getInstances()
    selectedInstances=[]

    for inst in instances1:
        for i in range(len(inst.form)):
            inst.pos[i]="_" # don't use column 5, only column 4 with UPOS
            if inst.cpos[i] not in mapping:
                print >>sys.stderr, "tag {} not in mapping".format(inst.cpos[i])
                exit()
            inst.cpos[i]=mapping[inst.cpos[i]]
        print inst

main()
