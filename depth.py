#!/usr/bin/python

""" 
Author: Barbara Plank
Date:   April 2015

Description: find max depth of tree


"""
import sys
import numpy as np
from conll.Conll07Reader import Conll07Reader
from optparse import OptionParser

def main():
    usage = "usage: %prog [options] file"

    parser = OptionParser(usage=usage)
    parser.add_option("-b", dest="b",default=False,action="store_true",help="per-sentence output")

    (options, args) = parser.parse_args()

    if len(args) < 1:
        print("Argument missing!")
        parser.print_help()
        exit(-1)
    
    fgold = args[0]
    
    readerGold=Conll07Reader(fgold)
    instancesGold=readerGold.getInstances()
    
    all=[]
    for instance in instancesGold:
        if options.b:
            print instance.maxTreeDepth()
        all.append(instance.maxTreeDepth())
        
    all=np.array(all)
    print "========== min, max, mean ========"
    print >>sys.stderr, all.min()
    print >>sys.stderr, all.max()
    print >>sys.stderr, all.mean()


main()
