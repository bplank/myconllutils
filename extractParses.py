#!/usr/bin/python3

""" 
Author: Barbara Plank
Date:   August 2011

Description: This script extracts sentences from a CoNLL file
of specific length

Options: specify minimun and maximum sentence length to extract

"""
import sys
from conll.Conll07Reader import Conll07Reader
from optparse import OptionParser

def main():
    usage = "usage: %prog [options] FILE"

    parser = OptionParser(usage=usage)
    parser.add_option("--minLen", dest="minLen",default=0,type="int",
                  help="extract sentences that have a minimum length")
    parser.add_option("--maxLen", dest="maxLen",type="int",
                  help="extract sentences up to this length")
    parser.add_option("--depRel", dest="depRel",type="string",
                  help="extract parses that contain this dep relation")
    parser.add_option("--word", dest="word",type="string",
                  help="extract parses that contain this word token")


    (options, args) = parser.parse_args()

    if len(args) < 1:
        print("Argument missing!")
        parser.print_help()
        exit(-1)
    
    file1 = args[0]

    reader1 = Conll07Reader(file1)

    instances1 = reader1.getInstances()

    for i in instances1:
        if i.getSentenceLength() > options.minLen:
            if options.maxLen:
                if i.getSentenceLength() <= options.maxLen:
                    if not options.depRel and not options.word:
                        print(i)
                    else:
                        if options.depRel:
                            if i.containsRelation(options.depRel):
                                if options.word:
                                    if i.containsWord(options.word):
                                        print(i)
                                else:
                                    print(i)
                        if options.word:
                            if i.containsWord(options.word):
                                print(i)
            else:
                if options.depRel:
                    if options.word:
                        if i.containsWord(options.word):
                            print(i)
                    else:
                        if i.containsRelation(options.depRel):
                            print(i)
                else:
                    if options.word:
                        if i.containsWord(options.word):
                            print(i)
                    else:
                        print(i)

main()
