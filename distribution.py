#!/usr/bin/env python
'''distribution.py
  
  Code to get distributions from data.

  Created by Vincent Van Asch on 27/11/09.
  Copyright (c) 2009 CLiPS. All rights reserved.'''

__author__='Vincent Van Asch'
__date__='27/11/09'

import os
import sys
#import tokenizer


class Distribution(dict):
    '''A distribution dictionary
    '''
    def __init__(self, values=[], id=None):
        self._total=0
        self._id = id
        self._computed=False
        if values:
            for v in values:
                self.add(v)
        
    def __repr__(self):
        parts = []
        for k,v in self.items():
            ks=k
            if isinstance(k,unicode):
                ks = k.encode('utf8')
            parts.append( '%s:%4.3f' %(ks, self[k]) )
        
        return '{%s}' %(', '.join(parts))
        
    def __str__(self):
        keys = self.keys()
        keys.sort()

        format = '%-15s\t%5d\t%6.5f'
        
        output=['%-15s\t%5s\t%-6s' %('key', 'count', 'freq')]
        for k in keys:
            ks=k
            if isinstance(k, unicode):
                ks = k.encode('utf8')
            output.append( format %(str(ks), self.count(k), self[k]))
        return '\n'.join(output)
        
        
    
    def __iter__(self):
        '''Yiels all probabilities
        i.e. self.values()/self.total sorted on the keys'''
        self.compute()
        keys = self.keys()
        keys.sort()
        
        for k in keys:
            yield self[k]
            
    def __getitem__(self, k):
        '''Returns the relative frequency of key k.
        0 if key is not in distribution'''
        self.compute()
        try:
            return dict.__getitem__(self, k)
        except KeyError:
            return 0
           
    @property
    def id(self):
        '''An id of the distribution.'''
        if self._id is None:
            raise ValueError('id attribute not set during initialization')
        return self._id
           
    @property
    def total(self):
        '''The total number of values that are
        in the distribution.
        
        Note that this is different from len().
        The length is the total number of unique
        values in the distribution
        '''
        if self._total == 0:
            self._total = sum(self.values())
        return float(self._total)
            
    def count(self, key):
        '''The absolute frequency of key i.e. the
        number of times it occurs in the values'''
        self.compute()
        try:
            return int(dict.__getitem__(self, key)*self.total)
        except KeyError:
            return 0
    
    def add(self, key):
        '''If called the count for key is increased with 1'''
        self[key] = self.get(key, 0) + 1
        self._total+=1

    def compute(self):
        '''Compute all relative frequencies and store those
        instead of the absolute counts'''
        self.total
        if not self._computed:
            for k,v in self.items():
                self[k] = v/self.total
        self._computed=True




def getdistributionfromfile(fname):
    '''Takes a fname and returns the distribution of the tokens(!)
    
    fname: a natural untokenized text
    '''
    try:
        tokenizer
    except NameError:
        print >>sys.stderr, 'To run this function you need a tokenizer.py. For example: http://www.clips.ua.ac.be/~vincent/software.html#tokenizer'''
        raise
    
    
    # Get the text
    f = open(fname, 'rU')
    txt = f.read()
    f.close()

    # Tokenize
    d = Distribution(id=fname)
    for sentence in tokenizer.split(txt):
        for token in sentence.split():
            d.add(token)    
    return d
    
    
    
def getdistributionfromcolumn(fname, field=0, sep=None, marker=None):
    '''Takes a file with columns and returns the distribution from the field.
    If a line doesn't contain the same number of fields as the first line it is omitted. 
    The rationale behind this is that this makes reading in MBT style files possible.
    
    sep: the columns separator (default: whitespace)
    marker: if set (e.g. <utt>) lines with only this marker are ignored
    
    
    '''
    f= open(os.path.abspath(os.path.expanduser(fname)), 'rU')
    d=Distribution(id=fname)
    length = None
    try:
        for l in f:
            line = l.strip()
            if line:
                if marker and line == marker: continue
                line = line.split(sep)
                if length is None:
                    length = len(line)
                if len(line) == length:
                    d.add(line[field])
    finally:
        f.close()
    return d

    
def getdistributiononclosedclassitemsfromcolumn(fname,stopwordfname, field=0, sep=None, marker=None,invert=False):
    '''Takes a file with columns and returns the distribution on closed class items from stopwordfname
    
    sep: the columns separator (default: whitespace)
    marker: if set (e.g. <utt>) lines with only this marker are ignored
    invert: if True, take only words which are NOT in stopwordfname into account
    
    '''
    f= open(os.path.abspath(os.path.expanduser(fname)), 'rU')
    fs= open(os.path.abspath(os.path.expanduser(stopwordfname)), 'rU')
    stopwords = [w.strip() for w in fs.readlines()]
    d=Distribution(id=fname)
    length = None
    try:
        for l in f:
            line = l.strip()
            if line:
                if marker and line == marker: continue
                line = line.split(sep)
                if length is None:
                    length = len(line)
                if len(line) == length:
                    if not invert and line[field] in stopwords:
                        d.add(line[field])
                    elif invert and line[field] not in stopwords:
                        d.add(line[field])
    finally:
        f.close()
    return d


def getdistributionfromsvm(fname, cutclass=False):
    '''Takes a file with instances as formatted for SVMLight
    i.e. sparse and class label is at field 0
    and returns the distribution of the instances.
    Note that the zeros are not in the actual distribution'''
    f= open(os.path.abspath(os.path.expanduser(fname)), 'rU')
    d=Distribution(id=fname)
    try:
        for l in f:
            line = l.strip()
            if line:
                # Cut off comments
                line = line.split('#')[0]
                # Cut off class label
                if cutclass:
                    line = ' '.join(line.split()[1:])
                else:
                    line = ' '.join(line.split())
                d.add(line)
    finally:
        f.close()
        
    return d
    
    
def getdistributionfromtimbl(fname, cutclass=True, sep=None, field=None):
    '''Takes a file with Timbl instances and returns the distribution'''
    f= open(os.path.abspath(os.path.expanduser(fname)), 'rU')
    d=Distribution(id=fname)
    try:
        for l in f:
            line = l.strip()
            if line:
                # Cut off class label
                if cutclass:
                    line = ' '.join(line.split(sep)[:-1])
                else:
                    line = ' '.join(line.split(sep))
                d.add(line)
    finally:
        f.close()
        
    return d


def getdistributionfromfreq(freq, id=None, total=100):
    '''Takes a list of frequencies and return the distribution.
    
    total: the number of elements
    
    Note that only supplying freq and not the total results in
    absolute counts that are equal to the 100*freq counts.
    '''
    d=Distribution(id=id)
    d._computed = True
    d._total = total
    for i, f in enumerate(freq):
        d[i] = f

    return d
    
    
    
def getinstancedistributionfromtimbl(fname, nfeatures, fields=None, sep=None, marker=None):
    '''Takes a Timbl file and uses the instances (minus the indices in the list fields)
    to build a distribution.
    
    nfeatures: the number of features in the instances.
    
    Note that the features in the keys are separated by \\x13
    '''
    # The features to keep
    keep = range(nfeatures)
    if fields:
        for fi in fields:
            keep.remove(fi)
    
    d = Distribution(id=fname)
    with  open(os.path.abspath(os.path.expanduser(fname)), 'rU') as f:
        for l in f:
            line = l.strip()
            if line and line != marker:
                parts = line.split(sep)
                
                key = '\x13'.join([parts[i] for i in keep])
                d.add(key)
                
    return d
                
                
                
                
                
                
                
                
                
                
    
    
