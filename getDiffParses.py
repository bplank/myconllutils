#!/usr/bin/python

""" 
Author: Barbara Plank
Date:   August 2011

Description: This script gets two CoNLL parse file and extracts parses that differ

Options: specify minimun and maximum sentence length to extract

"""
import sys
from conll.Conll07Reader import Conll07Reader
from optparse import OptionParser

def main():
    usage = "usage: %prog [options] FILE1 FILE2"

    parser = OptionParser(usage=usage)
    parser.add_option("--gold", dest="gold",type="str",
                  help="extract also gold data for diff samples")

    parser.add_option("--ignorePunct", dest="ignorePunct",default=False,action="store_true",
                  help="ignore punctuation when comparing trees")

    (options, args) = parser.parse_args()

    if len(args) < 2:
        print("Argument missing!")
        parser.print_help()
        exit(-1)
    
    file1 = args[0]
    file2 = args[1]

    if options.gold:
        readerGold=Conll07Reader(options.gold)
        instancesGold=readerGold.getInstances()


    # if options.alldiff and not options.gold:
    #     print >>sys.stderr, "Need also gold file!"
    #     exit()

    reader1 = Conll07Reader(file1)
    reader2 = Conll07Reader(file2)

    instances1 = reader1.getInstances()
    instances2 = reader2.getInstances()

    if not options.gold:
        diffs=[]
        assert(len(instances1)==len(instances2))
        for p1,p2 in zip(instances1,instances2):
            if not p1.sameParse(p2,ignorePunct=options.ignorePunct):
                diffs.append((p1,p2))

        for p1,p2  in diffs:
            print >>sys.stdout, p1
            print >>sys.stderr, p2
    
    if options.gold:
        diffs=[]
        assert(len(instances1)==len(instances2))
        assert(len(instances1)==len(instancesGold))
        for gold,p1,p2 in zip(instancesGold,instances1,instances2):
            p1.LAS(gold,ignorePunct=options.ignorePunct), p2.LAS(gold,ignorePunct=options.ignorePunct)
            # keep parses that are different from each other, but also don't score equal according to LAS as gold
            if not p1.sameParse(p2,ignorePunct=options.ignorePunct) and not p1.LAS(gold,ignorePunct=options.ignorePunct) == p2.LAS(gold,ignorePunct=options.ignorePunct):
                diffs.append((gold,p1,p2))

        FILE=open(file1+".diff.gold","w")
        for gold,p1,p2  in diffs:
            print >>sys.stdout, p1
            print >>sys.stderr, p2
            FILE.write(str(gold))
            FILE.write("\n")
        FILE.close()
#        print("gold file written: ", file1+".gold")

main()
