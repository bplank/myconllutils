#!/usr/bin/python

""" 
Author: Barbara Plank
Date:   August 2011

Description: This script gets two CoNLL parse file and extracts parses that differ

Options: specify minimun and maximum sentence length to extract

"""
import sys
from conll.Conll07Reader import Conll07Reader
from optparse import OptionParser
from collections import Counter,defaultdict
import numpy as np

def main():
    usage = "usage: %prog [options] file"

    parser = OptionParser(usage=usage)
    parser.add_option("-b", dest="b",default=False,action="store_true",help="per-sentence (evalb) output")
    parser.add_option("--all", dest="all",default=False,action="store_true",help="compute % for all groups, instead of --what")
    parser.add_option("--what", dest="what",default="core",help="distr of what (core, non-core, func)",choices=("core","func","non_core","punct"))
    (options, args) = parser.parse_args()

    if len(args) < 1:
        print("Argument missing!")
        parser.print_help()
        exit(-1)
    
    fgold = args[0]
    
    readerGold=Conll07Reader(fgold)
    instancesGold=readerGold.getInstances()

    if not options.all:
        total_cores = 0
        total = 0
        total_sentences_with_core_rels = 0
        total_arr_cores = []
        for instance in instancesGold:
            core_rels, total_rels, arr_cores = instance.getCoreDeprelCount(what=options.what)
            total_arr_cores.extend(arr_cores)
            if options.b:
                print(core_rels, total_rels, core_rels/float(total_rels))
            total_cores += core_rels
            total += total_rels
            if core_rels > 0:
                total_sentences_with_core_rels+=1

        print("total_cores: {} total_deprels: {} %core: {} %sents-with_core: {}".format(total_cores, total, total_cores/float(total),total_sentences_with_core_rels/float(len(instancesGold))))
        print("distr. core rel:", Counter(total_arr_cores))
    else:

        result = defaultdict(int)
        
        for instance in instancesGold:
            for what in ["core","func","non_core","punct"]:
                rels, total_rels, arr_cores = instance.getCoreDeprelCount(what=what)
                result['total_%s' % what] += rels
                if what == "core": # just count once
                    result['total'] += total_rels
                    if not 'core_distr' in result:
                        result['core_distr'] = []
                    result['core_distr'].extend(arr_cores)
        for what in ["core","func","non_core","punct"]:
            print("total_%s %s total: %s percent: %s" % (what, result[('total_%s' % what)], result['total'], result[('total_%s' % what)]/result['total']))
        counter = Counter(result['core_distr'])
        total = np.sum([counter[key] for key in counter.keys()])
        print(total)
        print(counter)
        counter_norm = {key: value/float(total) for key,value in counter.items()}
        print(counter_norm)
        #print(Counter(result['core_distr']))
main()
