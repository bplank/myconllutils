import argparse
import random
import sys
import numpy as np
from conll.Conll07Reader import Conll07Reader

def read_conll_file(file_name, file_format="pos"):
    """
    read in conll file
    word1    tag1
    ...      ...
    wordN    tagN

    Sentences MUST be separated by newlines!

    :param file_name: file to read in
    :return: generator of instances ((list of  words, list of tags) pairs)

    """
    current_words = []
    current_tags = []

    for line in open(file_name):
        line = line.strip()

        if line:
            if file_format == "pos":
                if len(line.split("\t")) != 2:
                    print("erroneous line: {} (line number: {}) ".format(line, i), file=sys.stderr)
                    exit()
                else:
                    word, tag = line.split('\t')
                current_words.append(word)
                current_tags.append(tag)
            if file_format == "nestedNER":
                if len(line.split("\t")) != 3:
                    print("erroneous line: {} (line number: {}) ".format(line, i), file=sys.stderr)
                    exit()
                else:
#                    print(line, line.split('\t'))
                    word, tag1, tag2 = line.split('\t')
                current_words.append(word)
                current_tags.append((tag1,tag2))
        else:
            yield (current_words, current_tags)
            current_words = []
            current_tags = []


    # check for last one
    if current_tags != []:
        yield (current_words, current_tags)

def write_conll(instances, outfile_name, format):
    OUT=open(outfile_name,"w")
    if format == "pos":
        for words, tags in instances:
            OUT.write("\n".join([u"{}\t{}".format(word, tag) for word, tag in zip(words, tags)]))
            OUT.write("\n\n")
    if format == "nestedNER":
        for words, tags in instances:
            OUT.write("\n".join([u"{}\t{}\t{}".format(word, tag1, tag2) for word, (tag1, tag2) in zip(words, tags)]))
            OUT.write("\n\n")

    else:
        for inst  in instances:
            OUT.write(str(inst))
            OUT.write("\n")
    OUT.close()

def main():
    parser = argparse.ArgumentParser(description="""sample from a POS tagged/PARSE conll file""")
    parser.add_argument("--format", help="pos or parse", choices=("pos","parse", "nestedNER"), default="pos")
    parser.add_argument("input", help="conll file")
    parser.add_argument("output", help="conll file")
    parser.add_argument("--seed", help="random seed", required=False, type=int)
    parser.add_argument("--k", help="select k instances", required=False, type=int)
    parser.add_argument("--p", help="select % instances", required=False, type=float)
    parser.add_argument("--t", help="select up to t number of tokens", required=False, type=int)
    parser.add_argument("--i", help="ignore first i instances", required=False, type=int)

    args = parser.parse_args()
    
    if args.seed:
        print("using seed: {}".format(args.seed), file=sys.stderr)
        random.seed(args.seed)

    instances = []
    num_tokens= 0
    
    if args.format in ["pos","nestedNER"]:
        for (words, tags) in read_conll_file(args.input, args.format):
            instances.append((words, tags))
            num_tokens += len(words)
    elif args.format == "parse":
        reader1 = Conll07Reader(args.input, args.format)
        instances = reader1.getInstances()
        num_tokens = np.sum([instance.getSentenceLength() for instance in instances])

    num_instances = len(instances)
    print("Loaded file {} with {} sentences".format(args.input,num_instances), file=sys.stderr)


    if args.k:
        if args.k > num_instances:
            print("k cannot be larger than {}. abort. ".format(num_instances))
            exit()
    if args.t:
        if args.t > num_tokens:
            print("t cannot be larger than {}. abort. ".format(num_tokens))
            exit()

    if args.i:
        instances = instances[args.i:] #ignore first


    random.shuffle(instances)
    
    if args.p: # e.g. 0.25
        # calculate num instances from percentage
        args.k = int(num_instances * args.p)

    if args.t:
        # calculate over num tokens rather than instances
        i=0
        curr_total=0
        if args.format in ["pos","nestedNER"]:
            for words, tags in instances:
                if curr_total >= args.t:
                    args.k= i
                    break
                curr_total+=len(words)
                i+=1
        else:
            for instance in instances:
                if curr_total >= args.t:
                    args.k= i
                    break
                curr_total+=instance.getSentenceLength()
                i+=1

    sample = instances[0:args.k]
    print("sampled {} instances. seed: {}".format(len(sample), args.seed), file=sys.stderr)
    write_conll(sample, args.output, args.format)


if __name__=="__main__":
    main()
