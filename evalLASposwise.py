#!/usr/bin/python

"""
Author: Hector Martinez Alonso (based on Barbara Plank)
Date:   April 2015

Description: This script gets two CoNLL parse file

"""
import sys
from conll.Conll07Reader import Conll07Reader
from optparse import OptionParser



def main():
    usage = "usage: %prog [options] gold sys"
    parser = OptionParser(usage=usage)
    (options, args) = parser.parse_args()

    if len(args) < 2:
        print("Argument missing!")
        parser.print_help()
        exit(-1)

    fgold = args[0]
    fsys = args[1]
    lang = args[2]



    postaglists = []
    for tag in 'ADJ ADP ADV AUX CONJ DET INTJ NOUN NUM PART PRON PROPN PUNCT SCONJ SYM VERB X'.split():
        postaglists.append([tag])
    nominalpos = "NOUN PRON PROPN".split()
    contentpos = "ADJ NOUN PROPN VERB".split()
    functionpos = "ADP ADV AUX CONJ DET PART PRON SCONJ".split()

    if lang == "da":
        postaglists = []
        for tag in ' AC AN AO CC CS I NC NP PC PD PI PO PP PT RG SP U VA VE XA XF XP XX'.split():
            postaglists.append([tag])
        nominalpos = "NP NC PP PD".split()
        contentpos = "AN NC NP VA VE".split()
        functionpos = "AC AO CC CS PC PD PI PO PP PT RG SP U".split()


    if lang == "hr":
        postaglists = []
        for tag in 'A C M N P Q R S V X Y Z'.split():
            postaglists.append([tag])
        nominalpos = "N P ".split()
        contentpos = "A N V".split()
        functionpos = "C Q M P R S ".split()

    postaglists.append(functionpos)
    postaglists.append(contentpos)
    postaglists.append(nominalpos)




    readerGold=Conll07Reader(fgold)
    instancesGold=readerGold.getInstances()

    reader1 = Conll07Reader(fsys)
    instances1 = reader1.getInstances()

    assert(len(instances1)==len(instancesGold))
    totalcorrectHead=0.0
    totalcorrectHeadDep=0.0
    totalalltokens=0.0
    totalscoringtokens=0.0
    for gold,p1 in zip(instancesGold,instances1):
        poslas = []
        for poslist in postaglists:
            alltokens,uas,las,correctHead,correctHeadDep,scoringtokens=p1.evaluateLASforPOS(gold,poslist=poslist,ignorePunct=True,b=False)
            poslas.append(las)

        print ",".join([str(x) for x in poslas])
        #for i,x in enumerate(poslas):
        #    print postaglists[i],x
    print >>sys.stderr, postaglists




main()
__author__ = 'alonso'
