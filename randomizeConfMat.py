import argparse
import sys
import random
from collections import Counter

parser = argparse.ArgumentParser(description="Randomized confmat generated with labelConfusionMatrix")
parser.add_argument("file",   metavar="FILE", help="name of the parsedfile file")

args = parser.parse_args()
#"VA	VA	1142.0	0.477025898079	0.987035436474	0.493517718237"


fin = open(args.file)

print fin.readline().strip(), "RANDOMIZED"
print fin.readline().strip()


keys = []
values = []
for line in fin.readlines():
    #"VA	VA	1142.0	0.477025898079	0.987035436474	0.493517718237"
    id1, id2, v1,v2,v3,v4 = line.strip().split("\t")
    keys.append(id1+"\t"+id2)
    values.append("\t".join([v1,v2,v3,v4]))


random.shuffle(values)

for k,v in zip(keys,values):
    print k+"\t"+v