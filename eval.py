#!/usr/bin/python

""" 
Author: Barbara Plank
Date:   August 2011

Description: This script gets two CoNLL parse file and extracts parses that differ

Options: specify minimun and maximum sentence length to extract

"""
import sys
from conll.Conll07Reader import Conll07Reader
from optparse import OptionParser

def main():
    usage = "usage: %prog [options] gold sys"

    parser = OptionParser(usage=usage)
    parser.add_option("-p","--ignorePunct", dest="ignorePunct",default=False,action="store_true",
                  help="ignore punctuation when comparing trees")
    parser.add_option("-b", dest="b",default=False,action="store_true",help="per-sentence (evalb) output")
    parser.add_option("-n", dest="ned",default=False,action="store_true",help="NED (Schwartz et al)")
    parser.add_option("-c", dest="c",default=False,action="store_true",help="complete predicates (SemEval-15)")
    parser.add_option("--cs", dest="cs",default=False,action="store_true",help="complete (soft) predicates: macro-avg of partial predicate matches")
    parser.add_option("--la", dest="la",default=False,action="store_true",help="label accuracy")
    parser.add_option("--LASbyDepth", dest="LASbyDepth",default=False,action="store_true",help="LAS weighted by root attachment distance")
    parser.add_option("--LASwPOS", dest="LASwPOS",default=False,action="store_true",help="LAS with VERB+NOUN counting twice")
    parser.add_option("--CNC", dest="CNC",default=False,action="store_true",help="CNC: canonical non-canonical (LAS on all relations except PUNCT and FUNC (=aux auxpass case cc cop det mark neg)")
    (options, args) = parser.parse_args()

    if len(args) < 2:
        print("Argument missing!")
        parser.print_help()
        exit(-1)
    
    fgold = args[0]
    fsys = args[1]

    
    readerGold=Conll07Reader(fgold)
    instancesGold=readerGold.getInstances()

    reader1 = Conll07Reader(fsys)
    instances1 = reader1.getInstances()

    if options.b:
        print(" Sent.          Attachment      Correct        Scoring")
        print(" ID Tokens  -   Unlab. Lab.   HEAD HEAD+DEPREL   tokens   - - - -")
        print("============================================================================")


    assert(len(instances1)==len(instancesGold))
    totalcorrectHead=0.0
    totalcorrectHeadDep=0.0
    totalalltokens=0.0
    totalscoringtokens=0.0
    for gold,p1 in zip(instancesGold,instances1):
        if options.ned:
            alltokens,uas,las,correctHead,correctHeadDep,scoringtokens=p1.evaluateNED(gold,ignorePunct=options.ignorePunct,b=options.b)
        elif options.c:
            alltokens,uas,las,correctHead,correctHeadDep,scoringtokens=p1.complete_predicates(gold,ignorePunct=options.ignorePunct,b=options.b)
        elif options.cs:
            alltokens,uas,las,correctHead,correctHeadDep,scoringtokens=p1.complete_predicates(gold,ignorePunct=options.ignorePunct,b=options.b,soft=True)
        elif options.la:
            alltokens,uas,las,correctHead,correctHeadDep,scoringtokens=p1.evaluateLA(gold,ignorePunct=options.ignorePunct,b=options.b)
        elif options.LASbyDepth:
            alltokens,uas,las,correctHead,correctHeadDep,scoringtokens=p1.LASbyDepth(gold,ignorePunct=options.ignorePunct,b=options.b)
        elif options.LASwPOS:
            alltokens,uas,las,correctHead,correctHeadDep,scoringtokens=p1.LASwPOS(gold,ignorePunct=options.ignorePunct,b=options.b)
        elif options.CNC:
            options.ignorePunct=True
            alltokens,uas,las,correctHead,correctHeadDep,scoringtokens=p1.evaluateCNC(gold,b=options.b)
        else:
            alltokens,uas,las,correctHead,correctHeadDep,scoringtokens=p1.evaluate(gold,ignorePunct=options.ignorePunct,b=options.b)
        totalcorrectHead+=correctHead       
        totalcorrectHeadDep+=correctHeadDep
        totalalltokens+=alltokens
        totalscoringtokens+=scoringtokens


    if options.ignorePunct:
        print("  ignoring punctuation")
        print("  Labeled   attachment score: {1:.0f} / {2:.0f} * 100 = {0:.2f} %".format( totalcorrectHeadDep/totalscoringtokens * 100,totalcorrectHeadDep,totalscoringtokens))
        print("  Unlabeled   attachment score: {1:.0f} / {2:.0f} * 100 = {0:.2f} %".format( totalcorrectHead/totalscoringtokens * 100,totalcorrectHead,totalscoringtokens))
    else:
        print("  Labeled   attachment score: {1:.0f} / {2:.0f} * 100 = {0:.2f} %".format( totalcorrectHeadDep/totalalltokens * 100,totalcorrectHeadDep,totalalltokens))
        print("  Unlabeled   attachment score: {1:.0f} / {2:.0f} * 100 = {0:.2f} %".format( totalcorrectHead/totalalltokens * 100,totalcorrectHead,totalalltokens))
    

main()
